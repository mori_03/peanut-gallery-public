# Applies to Everyone
* [TBC Raid Comp](https://tbcraidcomp.com/) - TBC Raid composition tool 1
* [wowtbc.gg](https://wowtbc.gg/) - TBC Raid Comp tool 2
* [seventyupgrades](https://seventyupgrades.com/) - set up your gearlists here for each phase of TBC classic
  * Can save your characters, sets
  * Shows character preview
  * Once list is saved shows complete list of heroics/dungeons/crafting mats needed for all gear
* [70.wowfan.net](https://70.wowfan.net/en/) - wowhead like database for TBC, very accurate, and has literally everything in very high quality
* TBC Dungeon Guides Done Quick:
  * [Shattered Halls Normal/HC](https://www.youtube.com/watch?v=EMkxP0QUtMU)
  * [Blood Furnace Normal/HC](https://www.youtube.com/watch?v=P5kcyWDvSw0)
  * [Hellfire Ramparts Normal/HC](https://www.youtube.com/watch?v=yRAaTp7SfKA)
* [Levelling guide for Classic TBC (Rep+Dungeon grind)](https://www.youtube.com/watch?v=0SrSyiQx6Kc)


# Paladins
* `LightClub - Paladin TBC` Discord: https://discord.gg/rvsyryAum7
  * Discord for discussion, theorycrafting, resources and everything about TBC Paladins

## Paladins - Protection
* `TBC Tankadin Sim`: https://crowdsourcegaming.github.io/TBC-Tankadin-Sim/
  * Sim for modeling some fights as a Prot Paladin
* `Walther's TBC tool`: https://docs.google.com/spreadsheets/d/1bZgJP837DJ_-aJMCqc2UtrLUmIRi4fgO-TsqQpsx3mI/edit#gid=809754556
  * Spreadsheet for some pre-raid BiS gear pieces


# Shamans
* [Advanced Shaman Guide for TBC (Defcamp/Melderon)](https://www.youtube.com/watch?v=z9zCM3lnkco)

