[[_TOC_]]
# Fire Resistance Gear
- When: Phase#2 (Tier 5)
- Why: Leotheras the Blind (Serpentshrine Caverns)
- Who: Tank only (warlock)
- How much: +365 (+230 without any buffs assuming warlock has Master Demonologist 5/5 and summoned Fel Hunter)

## Armor Pieces

### Neck
- Name: [Pendant of Frozen Flame](https://tbc.wowhead.com/item=24092/pendant-of-frozen-flame)
- Resistance: +30
- From: Jewelcrafter who's revered with Keepers of Time

### Chest
- Name: [Infernoweave Robe](https://tbc.wowhead.com/item=30762/infernoweave-robe)
- Resistance: +60
- From: G'eras (BoJ vendor)

### Legs
- Name: [Infernoweave Leggings](https://tbc.wowhead.com/item=30761/infernoweave-leggings)
- Resistance: +55
- From: G'eras (BoJ vendor)

### Hands
- Name: [Infernoweave Gloves](https://tbc.wowhead.com/item=30764/infernoweave-gloves)
- Resistance: +40
- From: G'eras (BoJ vendor)

### Boots
- Name: [Infernoweave Boots](https://tbc.wowhead.com/item=30763/infernoweave-boots)
- Resistance: +45
- From: G'eras (BoJ vendor)

## Summary
### Required Materials
- Badge of Justice (x100)

### Armor pieces
- Resistance gain: +230
- Neccessary buff: [Master Demonologist - Talent](https://tbc.wowhead.com/spell=23825/master-demonologist) + [Fire Resistance Totem (Rank IV)](https://tbc.wowhead.com/spell=25562/fire-resistance) or [Fire Resistance Aura](https://tbc.wowhead.com/spell=27153/fire-resistance-aura)
