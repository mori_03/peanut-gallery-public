[[_TOC_]]
# Frost Resistance Gear
- When: Phase#2 (Tier 5)
- Why: Hydross the Unstable (Serpentshrine Caverns)
- Who: Tanks only
- How much: +365 (+295 without any buffs)

## Armor Pieces
### Head
- Name: [Iceguard Helm](https://tbc.wowhead.com/item=31371/iceguard-helm)
- Resistance: +50
- From: Blacksmith who's honoured with The Violet Eye

### Neck
- Name: [Pendant of Thawing](https://tbc.wowhead.com/item=24093/pendant-of-thawing)
- Resistance: +30
- From: Jewelcrafter who's revered with Lower City

### Cloak
- Name: [Glacial Cloak](https://tbc.wowhead.com/item=22658/glacial-cloak)
- Resistance: +24
- From: Tailor who's revered with Argent Dawn

### Chest
- Name: [Iceguard Breastplate](https://tbc.wowhead.com/item=31369/iceguard-breastplate)
- Resistance: +60
- From: Blacksmith who's honoured with The Violet Eye

### Legs
- Name: [Iceguard Leggings](https://tbc.wowhead.com/item=31370/iceguard-leggings)
- Resistance: +60
- From: Blacksmith who's revered with The Violet Eye

### Ring
- Name: [The Frozen Eye](https://tbc.wowhead.com/item=31398/the-frozen-eye)
- Resistance: +35
- From: Jewelcrafter who's honoured with The Violet Eye

### Enchants/Kits
- Name: [Enchant Shield - Frost Resistance](https://tbc.wowhead.com/spell=13933/enchant-shield-frost-resistance)
- Resistance: +8
- From: Classic WoW - World Drop
----------------
- Name: Arcanum of Frost Warding
- Resistance: +20
- From: Tank himself who's revered with Keepers of Time
----------------
- Name: [Frost Armor Kit](https://tbc.wowhead.com/item=29486/frost-armor-kit) x1
- Resistance: +8
- From: Leatherworker (drops from the first boss in Steamvault)

## Summary
### Required Materials
- Khorium Bar (x26)
- Primal Water (x46)
- Primal Fire (x52)
- Primal Nether (x3)
- Felsteel Bar (x2)
- Mercurial Adamantite Bar (x5)
- Heavy Knothide Leather (x4)
- Large Radiant Shard (x1)
- Frost Oil (x1)
- Frozen Rune (x5)
- Bolt of Runecloth (x4)
- Essence of Water (x2)
- Ironweb Spider Silk (x4)

### Armor pieces
- Resistance gain: +295
- Neccessary buff: [Frost Resistance Totem (Rank IV)](https://tbc.wowhead.com/spell=25560/frost-resistance-totem)
- Progress: Tracked on Discord (#farming-priorities channel)
