[[_TOC_]]
# Shadow Resistance Gear
- When: Phase #3 (Tier 6)
- Why: Mother Shahraz (Black Temple)
- Who: Everybody
- How much: +365 (+295 assuming there's a priest in the raid group)

## Cloth (Mage, Priest, Warlock)

### Neck
- Item: [Medallion of Karabor](https://tbc.wowhead.com/item=32649/medallion-of-karabor)
- Resistance: +40
- Source: Reward from Black Temple attunement quest [A Distraction for Akama](https://tbc.wowhead.com/quest=10985/a-distraction-for-akama)

### Cloak
- Item: [Night's End](https://tbc.wowhead.com/item=32420/nights-end)
- Resistance: +40
- Source: Tailor who has Ashtongue Deathsworn - HONORED

### Wrists
- Item: [Soulguard Bracers](https://tbc.wowhead.com/spell=40021/soulguard-bracers)
- Resistance: +40
- Source: Tailor who has Ashtongue Deathsworn - FRIENDLY

### Waist
- Item: [Soulguard Girdle](https://tbc.wowhead.com/spell=40024/soulguard-girdle)
- Resistance: +54
- Source: Tailor who has Ashtongue Deathsworn - FRIENDLY

### Legs
- Item: [Soulguard Leggings](https://tbc.wowhead.com/spell=40023/soulguard-leggings)
- Resistance: +72
- Source: Tailor who has Ashtongue Deathsworn - HONORED

### Feet
- Item: [Soulguard Slippers](https://tbc.wowhead.com/spell=40020/soulguard-slippers)
- Resistance: +54
- Source: Tailor who has Ashtongue Deathsworn - HONORED

### Summary
- Total resistance: +300 (unbuffed)
- Required materials:
  - Heart of Darkness (x9)
  - Void Crystal (x5)
  - Primal Life (x13)
  - Primal Shadow (x18)
  - Bolt of Imbued Netherweave (x6)

## Leather (Rogue, Druid)

### Neck
- Item: [Medallion of Karabor](https://tbc.wowhead.com/item=32649/medallion-of-karabor)
- Resistance: +40
- Source: Reward from Black Temple attunement quest [A Distraction for Akama](https://tbc.wowhead.com/quest=10985/a-distraction-for-akama)

### Cloak
- Item: [Night's End](https://tbc.wowhead.com/item=32420/nights-end)
- Resistance: +40
- Source: Tailor who has Ashtongue Deathsworn - HONORED

### Wrists
- Item: [Redeemed Soul Wristguards](https://tbc.wowhead.com/spell=40004/redeemed-soul-wristguards)
- Resistance: +40
- Source: Leatherworker who has Ashtongue Deathsworn - HONORED

### Waist
- Item: [Redeemed Soul Cinch](https://tbc.wowhead.com/spell=40006/redeemed-soul-cinch)
- Resistance: +54
- Source: Leatherworker who has Ashtongue Deathsworn - FRIENDLY

### Legs
- Item: [Redeemed Soul Legguards](https://tbc.wowhead.com/spell=40005/redeemed-soul-legguards)
- Resistance: +72
- Source: Leatherworker who has Ashtongue Deathsworn - FRIENDLY

### Feet
- Item: [Redeemed Soul Moccasins](https://tbc.wowhead.com/spell=40003/redeemed-soul-moccasins)
- Resistance: +54
- Source: Leatherworker who has Ashtongue Deathsworn - HONORED

### Summary
- Total resistance: +300 (unbuffed)
- Required materials:
  - Heart of Darkness (x9)
  - Void Crystal (x5)
  - Primal Life (x13)
  - Primal Shadow (x18)
  - Bolt of Imbued Netherweave (x1)
  - Fel Hide (x11)

## Mail (Shaman, Hunter)

### Neck
- Item: [Medallion of Karabor](https://tbc.wowhead.com/item=32649/medallion-of-karabor)
- Resistance: +40
- Source: Reward from Black Temple attunement quest [A Distraction for Akama](https://tbc.wowhead.com/quest=10985/a-distraction-for-akama)

### Cloak
- Item: [Night's End](https://tbc.wowhead.com/item=32420/nights-end)
- Resistance: +40
- Source: Tailor who has Ashtongue Deathsworn - HONORED

### Wrists
- Item: [Bracers of Shackled Souls](https://tbc.wowhead.com/spell=40000/bracers-of-shackled-souls)
- Resistance: +40
- Source: Leatherworker who has Ashtongue Deathsworn - FRIENDLY

### Waist
- Item: [Waistguard of Shackled Souls](https://tbc.wowhead.com/spell=40002/waistguard-of-shackled-souls)
- Resistance: +54
- Source: Leatherworker who has Ashtongue Deathsworn - HONORED

### Legs
- Item: [Greaves of Shackled Souls](https://tbc.wowhead.com/spell=40001/greaves-of-shackled-souls)
- Resistance: +72
- Source: Leatherworker who has Ashtongue Deathsworn - HONORED

### Feet
- Item: [Boots of Shackled Souls](https://tbc.wowhead.com/spell=39997/boots-of-shackled-souls)
- Resistance: +54
- Source: Leatherworker who has Ashtongue Deathsworn - FRIENDLY

### Summary
- Total resistance: +300 (unbuffed)
- Required materials:
  - Heart of Darkness (x9)
  - Void Crystal (x5)
  - Primal Life (x13)
  - Primal Shadow (x18)
  - Bolt of Imbued Netherweave (x1)
  - Fel Scales (x11)

## Plate (Warrior, Paladin)

### Neck
- Item: [Medallion of Karabor](https://tbc.wowhead.com/item=32649/medallion-of-karabor)
- Resistance: +40
- Source: Reward from Black Temple attunement quest [A Distraction for Akama](https://tbc.wowhead.com/quest=10985/a-distraction-for-akama)

### Cloak
- Item: [Night's End](https://tbc.wowhead.com/item=32420/nights-end)
- Resistance: +40
- Source: Tailor who has Ashtongue Deathsworn - HONORED

### Wrists
- Item: [Shadesteel Bracers](https://tbc.wowhead.com/spell=40034/shadesteel-bracers)
- Resistance: +40
- Source: Blacksmith who has Ashtongue Deathsworn - FRIENDLY

### Waist
- Item: [Shadesteel Girdle](https://tbc.wowhead.com/spell=40036/shadesteel-girdle)
- Resistance: +54
- Source: Blacksmith who has Ashtongue Deathsworn - FRIENDLY

### Legs
- Item: [Shadesteel Greaves](https://tbc.wowhead.com/spell=40035/shadesteel-greaves)
- Resistance: +72
- Source: Blacksmith who has Ashtongue Deathsworn - HONORED

### Feet
- Item: [Shadesteel Sabots](https://tbc.wowhead.com/spell=40033/shadesteel-sabots)
- Resistance: +54
- Source: Blacksmith who has Ashtongue Deathsworn - HONORED

### Summary
- Total resistance: +300 (unbuffed)
- Required materials:
  - Heart of Darkness (x9)
  - Void Crystal (x5)
  - Primal Life (x13)
  - Primal Shadow (x18)
  - Bolt of Imbued Netherweave (x1)
  - Adamantite Bar (x28)
