[[_TOC_]]
# Nature Resistance Gear
- When: Phase#2 (Tier 5)
- Why: Hydross the Unstable (Serpentshrine Caverns)
- Who: Tanks only
- How much: +365 (+295 without any buffs)

## Armor Pieces
### Head
- Name: [Wildguard Helm](https://tbc.wowhead.com/item=31368/wildguard-helm)
- Resistance: +50
- From: Blacksmith who's honoured with Cenarion Expedition

### Neck
- Name: [Pendant of Withering](https://tbc.wowhead.com/item=24095/pendant-of-withering)
- Resistance: +30
- From: Jewelcrafter who's revered with The Scryers

### Chest
- Name: [Wildguard Breastplate](https://tbc.wowhead.com/item=31364/wildguard-breastplate)
- Resistance: +60
- From: Blacksmith who's exalted with Cenarion Expedition

### Legs
- Name: [Wildguard Leggins](https://tbc.wowhead.com/item=31367/wildguard-leggings)
- Resistance: +60
- From: Blacksmith who's revered with Cenarion Expedition

## Ring
- Name: [The Natural Ward](https://tbc.wowhead.com/item=31399/the-natural-ward)
- Resistance: +35
- From: Jewelcrafter who's exalted with Cenarion Expedition

## Enchants/Kits
- Name: [Enchant Cloak - Greater Nature Resistance](https://tbc.wowhead.com/spell=25082/enchant-cloak-greater-nature-resistance)
- Resistance: +15
- From: Enchanter who's honoured with Cenarion Circle (NOT Expedition)
----------------
- Name: Arcanum of Nature Warding
- Resistance: +20
- From: Tank himself who's honoured with Cenarion Expedition
----------------
- Name: [Nature Armor Kit](https://tbc.wowhead.com/item=29674/pattern-nature-armor-kit) x3
- Resistance: +8
- From: Leatherworker (drops from the first boss in Slave Pens)
----------------
- Name: [Inscription of Endurance](https://tbc.wowhead.com/item=29187/inscription-of-endurance)
- Resistance: +7
- From: Tank himself who's honoured with The Violet Eye

## Summary
### Required Materials
- Primal Life (x46)
- Primal Shadow (x60)
- Primal Nether (x3)
- Felsteel Bar (x28)
- Mercurial Adamantite Bar (x5)
- Heavy Knothide Leather (x12)
- Small Prismatic Shard (x2)
- Greater Plannar Essence (x3)
- Living Essence (x4)

### Armor pieces
- Resistance gain: +301
- Neccessary buff: [Nature Resistance Totem (Rank IV)](https://tbc.wowhead.com/spell=25574/nature-resistance-totem)
- Progress: Tracked on Discord (#farming-priorities channel)
