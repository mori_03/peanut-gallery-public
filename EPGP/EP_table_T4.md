[[_TOC_]]

# EP Value table for T4 content

In addition to the following tables, there's a `+10 EP` bonus for satisfying both of these conditions at the start of the raid:
* Having consumables
* Being on-time

## Karazhan
| Boss                     | EP Value |
| ------------------------ | -------- |
| Attumen the Huntsman     | +6       |
| Moroes                   | +6       |
| Maiden of Virtue         | +10      |
| Opera Event              | +10      |
| The Curator              | +10      |
| Terestian Illhoof        | +10      |
| Shade of Aran            | +10      |
| Netherspite              | +10      |
| Chess Event              | +6       |
| Prince Malchezaar        | +10      |
| Nightbane                | +12      |
| **FULL CLEAR**           | +10      |
| **FULL CLEAR (NO WIPE)** | +30      |
| **TOTAL**                | +110     |
| **TOTAL (NO WIPE)**      | +140     |


## Gruul\'s Lair
| Boss                     | EP Value |
| ------------------------ | -------- |
| High King Maulgar        | +30      |
| Gruul the Dragonkiller   | +30      |
| **TOTAL**                | +60      |
| **TOTAL (NO WIPE)**      | +80      |


## Magtheridon\'s Lair
| Boss                     | EP Value |
| ------------------------ | -------- |
| Magtheridon              | +60      |
| **TOTAL**                | +60      |
| **TOTAL (NO WIPE)**      | +80      |

