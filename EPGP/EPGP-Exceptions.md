- [EPGP Exception List](#epgp-exception-list)
  - [Tier 4](#tier-4)
    - [Tank Tier 4 BiS lists](#tank-tier-4-bis-lists)
    - [Tank Priority List](#tank-priority-list)
    - [Item Drops](#item-drops)
      - [`Void Crystals`](#void-crystals)
      - [`Soulcloth / Soul Essence`](#soulcloth--soul-essence)
      - [`Profession Recipes`](#profession-recipes)
  - [Tier 5](#tier-5)
  - [Tier 6](#tier-6)
  - [Sunwell](#sunwell)

# EPGP Exception List
This document is here to display a list of items that Tanks have priority for and items which go automatically to the guild bank.

## Tier 4

### Tank Tier 4 BiS lists
* Smikz
* [Teakay](https://seventyupgrades.com/set/uYT4HSNu45yhNknorvno2a)
* Koejo

### Tank Priority List
Our guild uses the EPGP system to distribute loot. As outlined from the start, our guild will have a public list of items which tanks will have priority on regardless of `EPGP PR` rating. Despite the priority, *tanks will still receive GP points for receiving these items.*

* Karazhan
  * [Helm of the Fallen Champion](https://tbc.wowhead.com/item=29760/helm-of-the-fallen-champion) `(Paladin/Rogue/Shaman)` - Prince Malchezzar
    * > Teakay (Protection Paladin)
  * 2x [Helm of the Fallen Defender](https://tbc.wowhead.com/item=29761/helm-of-the-fallen-defender) `(Warrior/Priest/Druid)` - Prince Malchezzar
    * > Smiikz (Protection Warrior)
    * > Koejo (Feral Druid)
* Gruul's Lair
  * [Pauldrons of the Fallen Champion](https://tbc.wowhead.com/item=29763/pauldrons-of-the-fallen-champion) `(Paladin/Rogue/Shaman)` - HKM
    * > Teakay (Protection Paladin)
  * 2x [Pauldrons of the Fallen Defender](https://tbc.wowhead.com/item=29764/pauldrons-of-the-fallen-defender) `(Warrior/Priest/Druid)` - HKM
    * > Smiikz (Protection Warrior)
    * > Koejo (Feral Druid)
  * 2x [Leggings of the Fallen Defender](https://tbc.wowhead.com/item=29767/leggings-of-the-fallen-defender) `(Warrior/Priest/Druid)` - Gruul
    * > Smiikz (Protection Warrior)
    * > Koejo (Feral Druid)
* Magtheridon's Lair
  * [Magtheridon's Head](https://tbc.wowhead.com/item=32385/magtheridons-head)
    * > Koejo (Feral Druid)
  * [Chestguard of the Fallen Champion](https://tbc.wowhead.com/item=29754/chestguard-of-the-fallen-champion) `(Paladin/Rogue/Shaman)` - Magtheridon
    * > Teakay (Protection Paladin)
  * 2x [Chestguard of the Fallen Defender](https://tbc.wowhead.com/item=29753/chestguard-of-the-fallen-defender) `(Warrior/Priest/Druid)` - Magtheridon
    * > Smiikz (Protection Warrior)
    * > Koejo (Feral Druid)

### Item Drops
This a list of items that automatically go into the guild bank, either all of them (materials), or just the first item (recipe).


#### `Void Crystals`
All 'useless' epics will be disenchanted into **Void Crystals** and will go to the guild bank. 

These `Void Crystals` will be used for:
* enchanting weapons Raiders receive in raids
* crafted gear
* sold for guild bank profits, *if in excess*

#### `Soulcloth / Soul Essence`
Soul Essence & Soul Cloth early on can be used as a cheap/alternative method of generating Void Crystals.

All of these items will be going to the guild bank / guild tailor.
* [Soul Essence](https://tbc.wowhead.com/item=21882/soul-essence)
* [Recipes using Soulcloth](https://tbc.wowhead.com/item=21844/bolt-of-soulcloth#reagent-for)

#### `Profession Recipes`
The first item of each will be going to the Guild Enchanter.
* [Enchanting: Mongoose](https://tbc.wowhead.com/item=22559/formula-enchant-weapon-mongoose)
* [Enchanting: Sunfire](https://tbc.wowhead.com/item=22560/formula-enchant-weapon-sunfire) 
* [Enchanting: Soulfrost](https://tbc.wowhead.com/item=22561/formula-enchant-weapon-soulfrost)


## Tier 5
TBA

## Tier 6
TBA

## Sunwell
TBA 