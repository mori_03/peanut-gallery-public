[[_TOC_]]

# EPGP Rules Overview - Raiding
- Being on time bonus is only granted to those who are inside the instance at 19:00 while having all necessary consumable ready
- Being *BENCHED* means in the raid group and ready to enter the instance within 5 minute notice (either in-game or on Discord)
- *BENCH* is for up to 2 people for 10man instance and up to 5 people for 25man instances
- Clearing the whole instance in a single night bonus is granted only when the raid finishes before 23:00
- None of the bonuses are exclusive (you can get all of them during a single raid night)

## EP Table
| Bonus                                         | EP Gain |
| --------------------------------------------- | ------- |
| Being on time + having consumables            | +10     |
| Clearing the whole instance                   | +5      |
| Clearing the whole instance in a single night | +5      |
| Clearing the whole instance without a wipe    | +10/+15 |
