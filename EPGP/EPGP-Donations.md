- [EPGP Bank Donations](#epgp-bank-donations)
  - [How to donate](#how-to-donate)
# EPGP Bank Donations
Every Raider/Trial has the opportunity to donate something to the guild bank for additional EP earned per week. This is entirely optional. EP gain from a donation to the guild bank is capped at `40 EP per week`.

You can, of course, donate more to the guild bank, but you will not earn more than the cap. 

> **Why are there donations at all?** In tier 5 there will be fights where resistance gear will be necessary, like the Hydross encounter in SSC. We intend to have full resistance sets prepared and ready for when the next phase hits for all tanks. We also want to provide expensive enchants for free for our raiders (Void Crystals), for free for a raid weapon (and later offspec/PVP)


| Item                                                                               | EP Value                                      |
| ---------------------------------------------------------------------------------- | --------------------------------------------- |
| [Void Crystal](https://tbc.wowhead.com/item=22450/void-crystal)                    | 20 / per item                                 |
| TBC BoE Crafting Recipe (first one only)                                           | 40 / per recipe                               | 
| Gold                                                                               | based on Void Crystal market value            |
| [Tank Frost-Res](./GameResources/Farming-Priorities/t5_frost_resistance_gear.md)   | based on Void Crystal market value            |
| [Tank Nature-Res](./GameResources/Farming-Priorities/t5_nature_resistance_gear.md) | based on Void Crystal market value            |

## How to donate
Send the the materials to the guild bank character **Trailmix** (Officer Alt), make sure to include whether items sent are for EP bonus:
<img src="./epgp_donation_example1.PNG">

So, for example, if you want to send two void crystals for an EP bonus to the guild bank, you'd do this:
<img src="./epgp_donation_example2.PNG">