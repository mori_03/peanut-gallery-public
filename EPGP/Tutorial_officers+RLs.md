[[_TOC_]]

# EPGP Tutorial for Officers & Raid Leaders
- Basics are already described in the [EPGP Tutorial for Raiders](EPGP/Tutorial_raiders.md)
- Notes below are for handling loot distribution, adding EP/GP manually and applying weekly decay

## Loot distribution
- Once you form a raid group and change the looting method to Master Loot, you have to confirm the use of EPGP

![EPGP - Enable](epgp_rl_usage.png "EPGP - Enabling loot distribution for the raid")

- Once you defeat a boss, EP will be distributed automatically
- After you loot the boss you'll see another EPGP window which will allow you to announce a piece of loot


![EPGP - Loot announcing](epgp_loot_distribution_1.PNG "EPGP - Loot announcing")

- After you announce it, everybody gets a window pop-up asking to select the option (including you). You'll also get a window which will live-update as people decide on their rolls and placing the prioritized rolls on top
- In the bottom left corner you can see an overview of the amount of people that already submited their response, wait for everybody
- The table is sorted based on the option (Main Spec/Off Spec, PvP Spec) and also PR value. So the person who gets the item should be on top. In case two people have the same value there's the roll column that works as a tie-breaker
- You distirbute the loot by left-clicking on the winner's name and then selecting the amount of GP you'd like the item to go for to the certain player

![EPGP - Loot distribution](epgp_loot_distribution_3.PNG "EPGP - Loot distribution")


## Weekly decay
- Happens every Monday/Tuesday whenever you get the chance to do it
- Prior to applying the decay you need to export the current standings:
  - Open the addon (`/cepgp show`) >> Guild >> Export standings
  - Make sure to include all info (Class, Guild Rank, Priority) and Uncheck the "Trailing Comma" option
  - Select "Format to JSON" - this will format the values and highlight it, simply do Ctrl+C and paste it into a text file and upload/send to Nihi
- Send a message to #epgp channel on Discord saying that the Decay has been applied

![EPGP - Decay](epgp_decay.png "EPGP - Decay")
![EPGP - Decay](epgp_weekly_decay.png "EPGP - Decay")

## Manual EP/GP modification
- In case you need to add any non-automated EP/GP
- Open addon, select "Guild" or "Raid" depending on what you need and simply left-click on the player or use option below to add to the whole guild/raid
- Always include a reason why it was awarded

![EPGP - Manual modifications](epgp_moderation.png "EPGP - Manual modifications")
