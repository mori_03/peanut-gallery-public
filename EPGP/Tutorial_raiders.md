# EPGP Tutorial for Raiders
1. Download Classic EPGP [here](https://www.curseforge.com/wow/addons/cepgp/files)
2. Install
3. Launch the game, you'll see a notification for first time users
4. Launch the addon (Use minimap icon or type `/cepgp show`)
5. Import the settings from Officer/RL
(Options >> Import Settings >> (Select All, fill in the name, Confirm)

![EPGP - Importing Config](epgp_import_config.png "EPGP - Importing Config")

## Get your current standing
For guild-wide standings select the Guild tab, similar for the raid-wide standings. As live updating doesn't always work you can use the following "commands" to check your standing:

- `!info` - Gets your current EPGP standing
- `!infoguild` - Gets your current EPGP standings and PR rank within your guild
- `!inforaid` - Gets your current EPGP standings and PR rank within the raid
- `!infoclass` - Gets your current EPGP standing s and PR rank among your class within the raid

You can whisper any of these keywords to the RL at any time to get the most up-to-date info.

![EPGP - Commands example](epgp_cmds.png "EPGP - Commands example")

## Minimum value/people on Trial

 - Minimum EP value you need to be able to roll for loot is 400 EP
 - This value (400 EP) was awarded to everybody at the start of our first raiding week, anybody else joining after this point is starting from 0
 - People on trial will not get any loot unless nobody else wants it and it would go for disenchant otherwise
 - Trials should be able to reach the 400 EP base value after attending 3-4 raids (~2 weeks)

## Loot distribution
Once RL announces a piece of loot from the boss, a window will pop-up on your screen and you can select one of the following:

- Main Spec (0% GP discount)
- Off Spec (99% GP discount)
- PvP Spec (99% GP discount)

![EPGP - Roll window](epgp_loot_distribution_2.PNG "EPGP - Roll window")

Based on response of each raid member the loot will get distributed. Here's the priority for loot:

1. Tank Priority - see [here](EPGP-Exceptions.md)
2. Main Spec
3. Off Spec
4. PvP Spec

And finally, as you'd expect a second set of priority:

1. Main character
2. Alt character
