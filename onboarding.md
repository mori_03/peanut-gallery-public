# Welcome to Peanut Gallery
Here's all the important stuff you need to know

## Required Addons
Every raider/trial in Peanut Gallery must install the following Addons:
* `a threat meter` Omen, DTM, whatever's popular and working. Examples:
  * [ThreatClassic2](https://www.curseforge.com/wow/addons/threatclassic2)
  * [Details! Damage Meter (TBC Version)](https://www.curseforge.com/wow/addons/details-damage-meter-burning-crusade)
* [CEPGP](https://www.curseforge.com/wow/addons/cepgp) (you don't have this you don't get loot)
* [Attune](https://www.curseforge.com/wow/addons/attune) (for keeping track of attunements guildwide)
* `Something like DBM/BigWigs` whatever you like better
  * [Deadly Boss Mods](https://www.curseforge.com/wow/addons/deadly-boss-mods)
  * [BigWigs](https://www.curseforge.com/wow/addons/big-wigs)

Additionally, healers must have the following installed:
* `Proper raid unit frames` like for example:
  * [Grid2](https://www.curseforge.com/wow/addons/grid2)
  * [Healbot Classic TBC](https://www.curseforge.com/wow/addons/healbot-classic-tbc)
* [Decursive](https://www.curseforge.com/wow/addons/decursive) (if your raid unit frames doesn't have such functionality)

All Paladins MUST have the following addons installed:
* [PallyPower](https://www.curseforge.com/wow/addons/pally-power) - for organizing blessings

## EPGP
To get started read our [EPGP Tutorial for Raiders](./EPGP/Tutorial_raiders.md). Here are some important links:
* [EPGP Table for T4](./EPGP/EP_table_T4.md) - lists all EPGP rewards, how, why and when
* [EPGP Exceptions](./EPGP/EPGP-Exceptions.md) - lists all exceptions to EPGP system (ex: tank priority for tier tokens)
* [EPGP Donations](./EPGP/EPGP-Donations.md) - you can get rewarded an EP bonus for donating specific stuff to the guild bank, to find out how, look here
* [EPGP RL/Officer Tutorial](./EPGP/Tutorial_officers+RLs.md) - for new and temp raidleaders, you'll need this
* [EPGP General Info](./EPGP/EPGP_Raiding_General.md) - general rules


## Discord Channels
General
* `#introductions` - introduce yourself here!
* `#guild-chat` - for random discussion
* `#class-discussion` - for focused discussion about classes, professions, dungeons, etc
* `#dungeon-crawling` - for announcing you're raiding 
* `#guild-bank-trailmix` - logs of guild bank donations & withdrawls

Dungeons/Raids
* `#epgp` - epgp announcements/discussion
* `#raid-signup` - here's where you sign up for raids
* `#raid-logs` - logs of past raids
* `#raiding-guides` 
* `#farming-priorities` 
