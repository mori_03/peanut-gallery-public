Welcome to public repository of Peanut Gallery [H][Firemaw-EU]

# Raiding Guides

## [Karazhan - Overview](./RaidingGuides/Karazhan/general.md)
- ### [Attumen the Huntsman](./RaidingGuides/Karazhan/boss1_attumen_the_huntsman.md)
- ### [Moroes](./RaidingGuides/Karazhan/boss2_moroes.md)
- ### [Maiden of Virtue](./RaidingGuides/Karazhan/boss3_maiden_of_virtue.md)
- ### [Opera Event](./RaidingGuides/Karazhan/boss4_opera_event.md)
- ### [The Curator](./RaidingGuides/Karazhan/boss5_curator.md)
- ### [Terestian Illhoof](./RaidingGuides/Karazhan/boss6_terestian_illhoof.md)
- ### [Shade of Aran](./RaidingGuides/Karazhan/boss7_shade_of_aran.md)
- ### [Netherspite](./RaidingGuides/Karazhan/boss8_netherspite.md)
- ### [The Chess Event](./RaidingGuides/Karazhan/boss9_chess_event.md)
- ### [Prince Malchezaar](./RaidingGuides/Karazhan/boss10_prince_malchezaar.md)
- ### [Nightbane](./RaidingGuides/Karazhan/boss11_nightbane.md)

## [Gruul's Lair - Overview](./RaidingGuides/GruulsLair/general.md)
- ### [High King Maulgar](./RaidingGuides/GruulsLair/boss1_high_king_maulgar.md)
- ### [Gruul the Dragonslayer](./RaidingGuides/GruulsLair/boss2_gruul_the_dragonslayer.md)

## [Magtheridon's Lair - Overview](./RaidingGuides/MagtheridonsLair/general.md)
- ### [Magtheridon](./RaidingGuides/MagtheridonsLair/boss1_magtheridon.md)

# Other

## [EPGP Exceptions](./EPGP/EPGP-Exceptions.md)
## [EPGP / Guild Bank Donations](./EPGP/EPGP-Donations.md)
## [Game Resources](./GameResources/)
- ### [TBC Consumables List](./GameResources/TBC%20Consumables%20List%20-%20Consumables%20-%20Notes.md)
- ### [Useful Game Link Resources](./GameResources/game_links_resources.md)
- ### [Farming Priorities](./GameResources/Farming-Priorities/)
  - #### [Tier5 - Fire Resistance Gear](./GameResources/Farming-Priorities/t5_fire_resistance_gear.md)
  - #### [Tier5 - Frost Resistance Gear](./GameResources/Farming-Priorities/t5_frost_resistance_gear.md)
  - #### [Tier5 - Nature Resistance Gear](./GameResources/Farming-Priorities/t5_nature_resistance_gear.md)