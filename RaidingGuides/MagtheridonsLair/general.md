# Magtheridon\'s Lair - Overview

[[_TOC_]]

## Preparation
- No attunement
- Beside bringing standard consumables consider capturing all 3 PvP towers in Hellfire Peninsula (The Overlook, The Stadium and Broken Hill) to gain buff which increases damage dealt by 5%

**Required Raid Composition**
- 3-5 Tanks
- 6-8 Healers
- 3 Warlocks (ideally 4)
- 3 Classes with Interrupt

## Trash
- You will meet 4 packs which will consist of 3 warlock-like mobs who deal heavy damage and reset aggro
- Respawns after 120 minutes
- 3 packs are stationary while one patrols around, don't try to fight two packs at once
- Immune to Silence and all forms of CC

### Hellfire Warder
- `Shadow Bolt Volley` - hits everyone in range with ~2k shadow damage
- `Shadow Word: Pain` - shadow DoT dealing 500 damage every 3 seconds
- `Unstable Affliction` - ticks for ~500 damage, deals ~3k damage if dispelled and silences the dispeller
- `Rain of Fire` - ~1900 fire damage every 2 seconds
- `Death Coil` - ~2000 shadow damage, lasts 3 seconds
- `Fear` - lasts 8 seconds, cannot be dispelled or removed with `Tremor Totem`
- `Shadow Burst` - deals ~2k shadow damage to the main aggro target, knocks back and reduces their threat

**Notes**
- Each one should have a separate tank and person interrupting `Shadow Bolt Volley` (ideally ranged as melee can get feared easily)
- Dispell `Shadow Word: Pain`
- DON'T dispell `Unstable Affliction`
- Ranged should stay far enough to avoid getting feared
- Be ready to reposition if hit by `Rain of Fire`
- Use `Mind-Numbing Poison` or `Curse of Tongues`
- Use `Grounding Totem` to absorb `Death Coil` or dispell it if used on tank
