# Magtheridon (Boss Encounter #1)

[[_TOC_]]

## Boss fight
- Doesn't start until one of the Hellfire Channelers is engaged
- Split into 3 phases

## Phase \#1 - Hellfire Channelers
- Starts by attacking any Hellfire Channeler
- Gives you 120 seconds before Magtheridon becomes active, kill as many Channelers as possible
- Kill Channelers one by one, focus on interrupting their casts (`Dark Mending` and `Shadow Volley`)
- Warlocks need to `Banish` or `Fear` each `Abyssal`

### Hellfire Channeler
- `Shadow Volley [1 second]` - up to 2k shadow damage
- `Fear [instant]` - single target, long range
- `Dark Mending [2 seconds]` - heals himself or another Channeler within 60 yards for up to 35k
- `Summon Abyssal [instant]` - summons infernal which despawns after 60 seconds
- `Soul Transfer [insant]` - when one Channeler dies, all surviving gain stacking buff increasing size by 20% + damage and casting speed by 30% with each stack

### Burning Abyssal
- Lands on random players
- Deals 2600-3400 damage within 10 yard range
- Casts `Fire Blast` dealing 3300 fire damage
- Despawns after 60 seconds
- Vulnerable to `Frost Nova`, `Banish`, `Fear` and all stuns or traps

**Notes**
- Each tank should have 1-2 Channeler on him
- Hunters need to help out with `Misdirection`
- Vulnerable to `Mind-Numbing Poison` and `Curse of Tongues`
- Stay away from Abyssals and keep them CC'd until they despawn

## Phase \#2 - Magtheridon's 100-30% health
- Starts after 120 seconds by Magtheridon breaking his Banish
- Kill any remaining Channelers first before moving onto Magtheridon

### Abilities
- `Cleave [instant]` - heavy frontal AoE
- `Conflagration [instant]` - small AoE zone cast on randomly across the room dealing 6000 fire damage over 8 seconds
- `Quake [instant]` - knockback that hits every player every second for 7 seconds; 50 second cooldown, first used after 40 seconds
- `Blast Nova [2 seconds]` - deals ~2,5k damage every 2 seconds for 10 seconds to everybody, 60 second cooldown
- `Enrage [instant]` - after 22 minutes

### Manticron Cubes
- Located on platforms where Hellfire Channelers were originally standing
- When clicked by a player, a channeling effects stars dealing 800 damage every 2 seconds to the clicker
- If all 5 cubes are activated at the same time, Magtheridon's `Blast Nova` while also increasing damage done to him to 300%
- Once channel breaks, the clicker receives `Mind Exhaustion` debuff preventing him to use the cube again for 30/60/120/180 seconds (go fuck yourself, Blizzard)
- Each time Magtheridon is about to cast `Blast Nova` you get a notification in the middle of your screen, click on the cube right when it appears
- Activation of the Cube is channeling, similar to Warlock's summoning, for simplicity just click ONCE and don't move
- Can be activated from any direction
- Use your offensive cooldowns right before `Blast Nova` (if you're not assigned to activating the cube) because of the increased damage

**Notes**
- Give the main tank enough time to build aggro as Magtheridon is immune to `Taunt`, help out with `Misdirection`
- Players assigned to Cubes can position themselves with their back against the wall near the Cube to avoid being knocked away by `Quake`
- If you won't survive the full channeling of the Cube, move out once `Blast Nova` is interrupted
- Avoid standing in `Conflagration` and in front of the boss because of `Cleave`

## Phase \#3 - Magtheridon's 30-0% health
- All the things which applied in Phase #2 are still true with a couple of additions
- Be sure not to enter this phase shortly before `Blast Nova` is about to beused, stop DPS completely if needed
- At 30% health Magtheridon shatters the walls of his lair causing the roof to crash down dealing 5300-6800 physical damage followed by 2 second stun to everybody (the damage and stun happens 15-20 seconds after Magtheridon shatters the walls)
- After that the ceiling will randomly caven in on players causing `Collapse` which deals heavy damage (there's a short cave-in animation which gives you time to move out of the way) - avoid it all costs
- Don't get to the Cube too early or `Collapse` might spawn on top of you preventing you from activating the Cube

## Class-specific notes

### Druid
- Use `Barkskin` if possible before clicking the Cube and before the ceiling crashes at ~30% as well

### Hunter
- Help out with `Misdirection` on Hellfire Channelers and once Magtheridon becomes active
- Can use `Freezing Trap` to control the infernals
- Mind about your pet positioning as he takes damage from `Collapse` and `Conflagration`

### Mage
- Can use `Frost Nova` to control the infernals
- Use `Ice Block` in the beginning of Phase #3 to avoid damage from the ceiling, be careful about the positioning as the animation starts about 15-20 seconds before the actual damage happens

### Paladin
- Use `Divine Shield` in the beginning of Phase #3 to avoid damage from the ceiling, be careful about the positioning as the animation starts about 15-20 seconds before the actual damage happens

### Priest
- Use `Fear Ward` on tanks so they don't get feared during Phase #1
- Pre-shield yourself to minimize the damage from the ceiling dropping as you might not survive the full hit

### Rogue
- Use `Mind-Numbing Poison` on Channelers, switch to damage dealing poisons before Phase #2 if possible
- Focus on interrupting Channeler's `Dark Mending` and `Shadow Volley`
- Use `Cloak of Shadows` before activating the cube to negate it's damage (?)

### Shaman
- Focus on interrupting Channeler's `Dark Mending` and `Shadow Volley`
- Use `Bloodlust` few seconds before `Blast Nova` (unless your group is assigned to activate it)
- Use `Shamanistic Rage` before the ceiling crashes at ~30%

### Warlock
- Use `Curse of Tongues` on Channelers
- Use `Banish` and `Fear` to control the infernals

### Warrior
- Focus on interrupting Channeler's `Dark Mending` and `Shadow Volley`

## Positioning
- Make sure everybody is in the room with Magtheridon before starting the fight as the gate will close once you engage the Channelers
- Tank Magtheridon with your back up against the wall to minimize `Cleave`
- Melee DPS should hit the boss from anywhere but his front
- Ranged DPS should spread to minimize possible damage from `Collapse` and `Conflagration`, ideally not too far from your assigned cubes
- Get to your assigned cube only a few seconds before you're supposed to activate it
- Negate the knockack of `Quake` by standing with your back up against the wall before activating the cube

### Phase \#1
- Example with 3 tanks

![Magtheridon - Positioning Phase #1](magtheridon_phase1.png "Magtheridon - Positioning Phase #1")

### Phase \#2 and 3

![Magtheridon - Positioning Phases #2 and 3](magtheridon_phase2-3.png "Magtheridon - Positioning Phases #2 and 3")
