# Netherspite (Boss Encounter #8 - Optional)

[[_TOC_]]

## Trash leading to Boss
- Same as those packs you faced on your way to Terestian Illhoof/Shade of Aran
- Pull one pack at a time while applying CC to make it easier

## Boss Fight
- Netherspite patrols in the back section of his room but has a small aggro radius which means raid can safely enter the room and position themselves
- Fight is purely tactical, other than `Enrage` timer there's no need to rush DPS
- Netherspite uses the following abilities:
  - `Netherburn` - aura (similar to Baron Rivendare's in Stratholme UD) dealing 1,2k shadow damage every 5 seconds, resistable, used in Phase #1
  - `Void Zone [channeled]` - opens a massive void portal that lasts 25 seconds, dealing ~1k shadow damage to everyone in the area, simply move out, used in Phase #1
  - `Netherbreath` - hits everyone within range of Netherspite dealing ~4,5k arcane damage and knocking back about 20 yards, used only in Phase #2
  - `Empowerement [3 seconds]` - undispellable self-buff making Netherspite immobilized an but buffs his damage by 200%, casted at the beginning of Phase #2
  - `Enrage [instant]` - increases Netherspite's damage by 500%, used after 9 minutes
- Netherspite doesn't have cleave nor tail damage, unlike most dragons
Fight is split into two phases which alternate between each other until Netherspide dies or gets `Enrage`

## Phase \#1 (Beam Phase)
- Fight begins in this phase
- Lasts 60 seconds
- Players get system warning saying: `Netherspite cries out in withdrawal, opening gates to the nether.`
- Three portals of different colours will spawn at fixed locations and begin emitting a beam of their respecitve colour towards Netherspite
- If beams are allowed to hit Netherspite he gains stacking buffs which strengthen or heal him (depending on the colour)
- Buffs on Netherspite last 5 seconds from last application (8-20 seconds on players)
- Beams must be blocked by players by positioning themselves between the portal and Netherspite
- Once buff ticks out (player leaves the beam long enough to lose the debuff [8-20 seconds]) another debuff to the player is applied called `Nether Exhaustion`, which prevents him from intercepting that colour beam for 90 seconds
- Portals will spawn at the same locations in the room during all Beam phases, but their colours will semi-randomly change each time

### Red Beam (Perseverence - Tank Beam)
- Lasts 20 seconds after leaving the beam
- Hits Netherspite:
  - Damage taken is reduced by 1% per tick
- Hits player:
  - Netherspite will aggro you (if no one is blocking it, Netherspite uses standard aggro table)
  - Damage taken is reduced by 1% per tick
  - Defense is increased by 5 per tick
  - Health is modified (first application gives +31k maximum health, additional stacks reduce maximum health by 1k per tick)
  - Replenishes full health every tick

### Green Beam (Serenity - Healer Beam)
- Lasts 10 seconds after leaving the beam
- Hits Netherspite:
  - Heals for +4000 per tick (stacking, so it's +4k, +8k, +12k, ...)
- Hits player:
  - Healing done is increased by 5% per tick
  - Spell and ability cost is reduced by 1% per tick (meaning mana, energy and rage)
  - Maximum mana is reduced by 200, stacks up until your total mana pool gets to 0 (affects shapeshifted druids as well)
  - Mana is restored to the new maximum each tick

### Blue Beam (Dominance - DPS Beam)
- Lasts 8 seconds after leaving the beam
- Hits Netherspite:
  - +1% spell damage increase per tick
- Hits player:
  - Damage dealt is increased by 5% per tick
  - Healing received is reduced by 1% per tick
  - Damage taken by spells is increased by 8%

### Beam blocking
- This part of the fight relies on individual player and he needs to be ready ahead of time to interecept it instantly
- Those not blocking the red beam either deal damage or heal the raid
- Healers shouldn't waste time healing the tank hit by the red beam
- DPS don't have to worry about threat
- Each blocked needs to be careful if `Void Zone` appears on top of him and should move forward or backward, never sideways as you might risk stepping out of the beam

**Red Beam blocking**
- Classes with high armor should block it
- Can be either blocked by two players (first one for 25 ticks, second one till the rest of the phase) or by a single tank dancing in and out
- If one player takes the entire red beam, they must be a dedicated tank (or someone with a shield if necessary) and should repeatedly take ~5 stacks, then step to the side and let Netherspite take 3-4 stacks, then step back in

**Green Beam blocking**
- Ideally blocked by a manaless class (warrior, rogue, feral druid) because he can stay for the whole duration, tank who's not blocking the red beam is a solid choice
- If the above isn't possible or healers aren't able to keep the Blue Beam blockers alive, get a healer to block for 30 seconds

**Blue Beam blocking**
- Taken by any DPS class, preferably with high stamina and/or shadow resistance
- 2 players should block it during one phase, the first one for 25 ticks, the other till the end of the phase
- Warlock is the perfect choice since he can replenish health using `Drain Health` and is able to reduce shadow damage taken, another good choice is a shadow priest due to their `Vampiric Embrace` which scales greatly with the Blue Beam buff
- Death of a blue beam blocker doesn't have to mean a wipe, as any other DPS can step in and block the beam for the duration

## Phase \#2 (Banish Phase)
- Lasts for ~30 seconds
- Players get system warning saying: `Netherspite goes into a nether-fed rage!`
- Netherspite will turn his colour to the one of a `Banished` mob, will remain stationary but vulnerable to damage
- Netherspite has no aggro table and is still able to deal melee damage if approached
- After a few seconds of inactivity, Netherspite targets a random player and will attempt to cast `Netherbreath` on him, ability has about 60 yard range and effects all players in a frontal cone

### Strategy
- Form a circle around Netherspite and continue attacking (except for melee DPS), when Netherspite targets a player to cast `Netherbreath` he should be the only one hit and get knocked back, he should heal/bandage and run back to his previous spot
- After 30 seconds Netherspite starts a fresh Phase #1 and clears aggro table, meaning that no healing/damaging can be done with tank gets hold of the boss
- Tank should pick up Netherspite and drag him over to the red portal or healers will get stuck, others should run to where their portals have appeared as quickly as possible

## Class-specific notes

### Druid
- Your mana continues to be drained when blocking Green Beam, be sure not to get stuck shapeshifted because of it
- Be ready to use `Rebirth` and `Innervate` quickly if needed

### Hunter
- If you use `Feign Death` when blocking a beam, you receive the `Nether Exhaustion` debuff and possible cause a wipe
- Help out with `Misdirection` in the beginning of each Beam Phase, but be careful to use it on the current Red Beam blocker

### Paladin
- Use `Shadow Protection Aura` if there's no priest providing the resistance buff, because the tanks hit by red beam should have enough stats and not require `Devotion Aura`

### Priest
- Buff both groups with `Prayer of Shadow Protection`
- Shadow Priests can sustain a full duration of the Blue Beam if they have 2/2 `Improved Vampiric Embrace`, this also does a huge amount of healing for his group and off-loading the healers

### Shaman
- Use `Bloodlust` after first Banish Phase once tank has enough aggro
- Be ready to leave `Searing Totem` close to Netherspite but outside of his melee range during Banish Phases if you go with that Strategy

### Warlock
- Use `Curse of Doom` at the end of the portal phase when you have meany Blue Beam buffs stacked, it can hit for 5-7% his health
- Use `Drain Health` when blocking Blue Beam to help out the healers
- Use `Shadow Ward` as much as possible to soak up damage

## Positioning
- Players assigned to block beams need to be quick during phase transitions to prevent Netherspite from getting stacks of the buff/heal up
- Portal spawn at the same location but their colours change each Beam Phase, be ready to get to it immediately
- Players assigned to block the Blue Beam should be standing behind each other, with the first blocker closer to the portal and the second in front of him (see picture), once the first one gets 23-25 stacks simply step aside
- Do NOT interrupt any beam you're not assigned to at ANY cost
- Positioning below is an example, the colours of portal in each location could change in each Beam Phase, however the one shown should be the initial setup
- Netherspite doesn't have cleave or tail damage, like most dragons, so you can position wherever you want while avoiding interrupting beams assigned to others

![Netherspite - Positioning (P1)](netherspite.png "Netherspite - Positioning (P1)")
