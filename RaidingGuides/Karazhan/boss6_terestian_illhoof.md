# Terestian Illhoof (Boss Encounter #6 - Optional)

[[_TOC_]]

## Trash leading to the boss
- Some of the packs will be the same as the ones you fought on your way to Curator
- Arcane Protectors have reflective abilities, need to be kited when `Fist of Stone` is used
- Mana Warps should be stunned at 10% health and killed quickly otherwise they will explode doing around 5.5k arcane damage within 40 yards
- Sorcerous Shade have various Volleys (Frostbolt, Shadowbolt, Fireball) and should be killed first
- Ethereal Thief - uses `Disarm` on the first aggro target while lowering threat at the same time, need to be off-tanked and killed individually
- Ethereal Spellfilcher has a spell power self-buff which needs to be purged/dispelled/stolen
- In general use any CC available: `Banish`, `Shackle Undead` or `Freezing Trap` and pull the smallest group possible

## Boss Fight
- In addition to facing Illhoof you'll also have to fight his demon - Kill'rek together with the imps that will spawn from the portals during the fight
- Terestian Illhoof's abilities:
  - `Shadow Bolt [1,5 seconds]` - deals around 4000 shadow damage at the highest aggro target
  - `Sacrifice [1 second]` - random player is summoned to the altar in the middle of the room and is paralyzed taking 1500 unresistable damage every second until the Demon Chains are destroyed, each second Demon Chains are up, Illhoof is healed for 3000 health, cannot target the highest aggro target
  - `Berserk [instant]` - after 10 minutes
- Kil'rek's abilities:
  - `Amplify Flames [1 second]` - increases fire damage taken by an enemy by 500 for 25 seconds
  - `Broken Pact [instant]` - all damage taken is increased by 25% (cast on Illhoof on death)
- Kil'rek respawns every ~45 seconds in the center of the room
- After approx. 10 seconds into the fight Illhoof summons two portals in the back of the room and Imps will start pouring out of them in a steady rate
- DPS' main priority should be Demon Chain and using any AoE abilities available do deal with the Imps
- Imps need to be constantly damaged before they overwhelm the room causing a wipe, ideally by Warlocks `Seed of Corruption`
- If raid DPS is high enough consider killing Kil'rek first to get damage buff onto Illhoof, otherwise let him die to AoE damage
- Should be tanked by a Protection Paladin positioned between the two portals and close to the altar to hit all the imps with his `Consecration`, this also means melee can quickly turn around to damage down Demon Chains
- If Kil'rek isn't being killed first he should be handled by an OT right next to Illhoof so he can be cleaved down
- Illhoof targets a random player before using `Sacrifice` which gives healers time to setup HoTs, absorbs and pre-cast heals

## Class-specific notes

### Druid
- Use `Barkskin` when Illhoof starts casting `Sacrifice` on you
- Use `Hurricane` to damage down the imps in case there's too many of them
- Make sure to keep `Thorns` on the main tank for extra damage, ideally `Improved Thorns` from a Balance Druid

### Hunter
- Pet with high stamina can be assigned to tank Kil'rek assuming he gets enough heals
- Use `Snake Trap` on top of the tank as Illhoof can cast `Sacrifice` on one of them (?)
- Help out with `Misdirection` to help establish aggro quickly
- Keep `Aimed Shot` debuff on Illhoof to minimize his healing during `Sacrifice`

### Mage
- Use `Ice Block` to get out of `Sacrifice`
- Use any AoE abilities to help deal with the imps

### Paladin
- Use `Fire Protection Aura` when in a group with the tank (or the tank himself) and `Concentration Aura` when group with AoE DPS
- Use `Divine Shield` to get out of `Sacrifice`
- Holy Paladins can help out with damage using `Consecration` on top of the tank

### Priest
- Buff both groups with `Prayer of Shadow Protection`

### Rogue
- Keeping `Wound Poison` on Illhoof helps minimizing his healing during `Sacrifice`
- Use `Blade Flurry` to cleave down the imps/Kil'rek

### Shaman
- Use `Magma Totem` and `Fire Nova Totem` to help dealing with the imps and `Fire Resistance Totem`
- Use `Bloodlust` once tank has enough aggro
- Elemental Shamans should use `Chain Lightning` as much as possible without risking running out of mana too quickly

### Warlock
- Spamming `Seed of Corruption` to deal with the imps should be your main priority


### Warrior
- Keeping `Mortal Strike` debuff on Illhoof helps minimizing his healing during `Sacrifice`
- Use `Sweeping Strikes` and `Cleave` to help with the imps

## Positioning
![Terestian Illhoof - Positioning](illhoof.png "Terestian Illhoof - Positioning")
