# Attumen the Huntsman (Boss Encounter #1 - Optional)

[[_TOC_]]


## Trash leading to the boss
- Respawn timer of 25mins tied to Attumen
- Spectral Stable Hands should be your primary target as they can use `Mend` on horses
- All packs should be cleared since Midnight calls any surrounding horses when engaged
- Use `Shackle Undead` or `Freezing Trap` if necessary

## Boss Fight
- Simple spank-and-tank split into 3 phases
- Midnight doesn't attack unless being engaged which makes clearing the trash packs around her easier
- Whole fight can be reset by running to the Entrance Hall

### Phase \#1 - Midnight (100 - 95%)
- No abilities, just waiting for aggro
- Tanked by off-tank (Prot Paladin/Feral Druid)


### Phase \#2 - Attumen and Midnight (until either reaches 25%)
Attumen will appear near Midnight and starts attacking right away and will use the following abilities:
- `Shadow Cleave [instant, 180 degree frontal AoE]`
- `Intangible Presence [instant, curse debuff, cast every 30 seconds]` - Curse reducing hit with ranged/melee attacks and spells by 50%
- `Mount Up [instant]` - When either him or Midnight get to 25% he will mount up, turning into one boss with higher health pool
- `Berserker Charge [instant]` - charge dealing minor damage but can be followed by an auto-attack which could potentially one-shot cloth classes, followed by a knockdown. Hits random target at 8-40 yards (only used in Phase #3)


**Notes**
- Hunter should help with `Misdirection` onto the MT (ideally a Prot Warrior)
- DPS and Healers should stop doing anything before enough aggro is established
- Attumen should be moved a few yards away from Midnight to avoid cleave
- Midnight is the primary target
- Both Midnight and Attumen are immune to `Taunt`


### Phase \#3 - Mounted Attumen
- Begins when either Midnight or Attumen is brought to 25% health
- Attumen will `Mount Up` and becomes a single unit together with Midnight
- Threat table is wiped, so MT needs a second to establish threat, Hunter should help out with `Misdirection`
- Attumen's `Berserker Charge` has minimum range of 8 yards which means everybody should stack behind him (including the OT) to avoid the ability from being used
- OT should swap to dealing damage


## Class-specific notes

### Druid
- Apart from Feral-spec decursing if your primary responsibility during second and third phase, priority should be the tanks and then dps (healers don't have to be decursed at all)

### Hunter
- Help out with `Misdirection` at the beginning of Phase #2 (on Attumen) and Phase #3
- During Phase #3 you should stack behind the boss and the minimal range of Attumen's charge is 8 yard which should be enough for you to deal damage
- Make sure your pet is not standing next to the tank to avoid cleave damage
- Keep `Scorpid Sting` active on both targets

### Mage
- Decursing is your primary responsibility during second and third phase, priority should be the tanks and then dps (healers don't have to be decursed at all)

### Shaman
- Use Bloodlust in Phase #3 once tank has threat under control

### Warrior
- If you're in charge of tanking Attumen you should try to use `Spell Reflect` right before he's about to cast `Intangible Presence` as it can be bounced back onto the boss and weaken him considerably
- Use `Disarm` on Attumen to lower his damage output

## Positioning
![Attumen - Positioning](attumen.PNG "Attumen - Positioning")
