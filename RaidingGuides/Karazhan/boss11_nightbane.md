# Nightbane (Boss Encounter #11 - Optional)

[[_TOC_]]

## Trash leading to the boss
- Already covered in previous guides because Nightbane is summoned on The Master's Terrace which is close to Opera House

**Notes**
- In order to summon Nightbane someone in the raid needs to have `Blackened Urn` from completing the questline starting with [Medivh's Journal](https://tbcdb.com/?quest=9630) that becomes obtainable after reaching Honoured with The Violet Eye
- Once `Blackened Urn` is used you have ~30 seconds before the fight begins to position  yourself

## Boss fight
- Is cyclical and features two repeating phases depending on Nightbane's health
- Fight is very long (~10 minutes) so make sure not to save any consumables and cooldowns for later, you might get to use them multiple times
- Ground Phase is where most of the fight happens and is similar to most encounters with dragons
- At 75%, 50% and 25% health Nightbane takes to the sky during which time the raid focuses on killing his adds, once he finishes casting those abilities he will land back onto the terrace again
- Immune to `Taunt`, dump aggro **EVERY** chance you get

### Ground Phase
- Uses the following abilities:
  - `Bellowing Roar [2,5 seconds]` - AoE fear used every 45-60 seconds
  - `Charred Earth [1 seconds]` - chars the ground beneath a random player, all players standing inside the affected area get 3000 fire damage per second, lasts 30 seconds, used randomly
  - `Cleave [instant]` - hits between 6-11k depending on armor
  - `Distracting Ash [1 second]` - reduces chance to hit with melee and spells by 30% for 40 seconds, dispellable
  - `Smoldering Breath [2 seconds]` - inflicts 5-6k fire damage to enemies in frontal cone and deals 1700-1900 damage every 3 seconds
  - `Tail Sweep [instant]` - inflicts 450 fire damage to enemies in a cone behind the boss, knocking them back, dels 450 fire damage every 3 seconds for 25 seconds
- Tank should be positioned with his back against the wall on the outside edge of the Terrace
- Melee DPS should be damaging between his legs from the side to avoid `Cleave` and `Tail Swipe`
- Ranged DPS + Healers should be split into two groups, one on each side of the balcony in order to minize `Charred Earth` damage

### Air Phase
- Begins upon Nightbane losing 25% health (at 75, 50 and 25%)
- Only uses the following abilities:
  - `Rain of Bones [instant]` - targets random player/pet, bombards the area around the targeted person dealing 300-400 damage in 6 yard radius and summons 5 `Restless Skeletons` each hitting with melee attack for 500-1300 and 13.5k health
  - `Smoking Blast [1 second]` - shoots one every 15 seconds, dealing ~2k physical damage (on cloth) to the target and 3000 fire damage over 18 seconds, the DoT is dispellable, targets the first player in the threat table (usually a healer)
  - `Fireball Barrage [instant]` - hits anybody who's more than 40 yards away from Nightbane, each blast dealing 3-3,5k damage, easily avoidable
- Right after the `Restless Skeletons` are summoned Nightbane will start casting `Smoking Blast` onto the highest aggro target making him the primary heal target
- While healers focus on keeping the highest aggro target alive, tanks need to focus on getting aggro on the skeletons and DPS on destroying them quickly
- Tanks need to get aggro as quickly as possible, this means using AoE Taunt, Engineering Grenades, Stratholme Holy Water
- DPS should help out with `Frost Nova`, `Frost Trap`, `Earthbind Totem`, etc.


### Phase transition
- About 10 seconds before Nightbane lands the raid should get into position, if there are are `Restless Skeletons` left they need to dealt with before moving back to Nightbane
- Nightbane resets aggro in the beginning of each phase, so DPS and healers need to give tank some time at the start of each Ground Phase

## Class-specific notes

### Druid
- Don't hesitate to use `Innvervate` early on, as you might use it several times throughout the whole fight
- Use `Barkskin` when targetted by `Smoking Blast` if possible
- Ferals/Balance druid should help out when `Restless Skeletons` by using `Hurricane` to lower their attack speed
- Feral tanks shouldn't hesitate to use `Challenging Roar` if skeletons start getting loose

### Hunter
- Use `Misdirection` on the main tank at the beginning of each ground phase
- Pets can be targetted by `Charred Earth`, as well as `Tail Swipe` and `Cleave` - be careful about his positioning
- Help out with `Frost Trap` to slow down the `Restless Skeletons` and make controlling them easier, preferably with `Entrapment` talents

### Mage
- Bursting down the skeletons is your priority, help control them with `Frost Nova`, `Cone of Cold`, etc.

### Paladin
- Prot Paladin should use his offensive cooldowns to get aggro on `Restless Skeletons`
- If you have a Prot Paladin consider using `Blessing of Wisdom` instead of `Blessing on Salvation` as getting hold of the skeletons should be a problem

### Priest
- Keep using `Fear Ward` on the tank throughout the whole fight
- Don't be afraid to use `Shadowfiend` early on as you might get to use it twice

### Shaman
- Have `Tremor Totem` down during the whole Ground Phase
- Help out with `Earthbind Totem` to slow down the skeletons or even `Stoneclaw Totem` if you don't have a Prot Paladin tanking them
- Use `Bloodlust` during the second Ground Phase once tank gets enough aggro

### Warlock
- Bursting down the skeletons is your priority, once the tank has aggro spam `Seed of Corruption`

### Warrior
- Once Nightbane starts casting `Bellowing Roar` get ready to stance-dance out of it, especially if you're tanking
- Don't hesitate to use `Challenging Shout` if the skeletons start getting loose

## Positioning
- When targeted by `Charred Earth` simply move to the other side of the terrace
- In case both sides of the terrace are covered with `Charred Earth` simple move forward towards Nightbane but make sure  you don't get hit by `Cleave` or `Tail Swipe`
- Melee DPS and pets should be dealing damage from the side to avoid `Cleave` and `Tail Swipe`
- If Melee get targeted by `Charred Earth` they should move to the other side of the boss while staying in range of the healers
- Main tank should be standing with his back against the wall and be ready to turn the boss around quickly if targeted by `Charred Earth`
- Ranged should be far enough to avoid `Bellowing Roar` (35 yards) (? range)
- Below is an ideal setup during Ground Phase

![Nightbane - Positioning during Ground Phase](nightbane.png "Nightbane - Positioning")
