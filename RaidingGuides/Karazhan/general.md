# Karazhan - Overview

[[_TOC_]]

## Preparation
- Everybody needs to be attuned and have The Master's Key in their bags to be able to enter (at least in Phase #1 of TBC Classic)
- Lot of trash packs and some bosses are Undead which makes trinkets from  Stratholme quest [The Active Agent](https://classic.wowhead.com/quest=5213/the-active-agent) very useful, even better alternatives are their upgraded version from Naxxramas  quest [The Fall of Kel'Thuzad](https://classic.wowhead.com/quest=9120/the-fall-of-kelthuzad)

**Required raid composition**
- 2 Tanks (ideally Prot Warrior + Prot Paladin/Feral Druid)
- 2-3 Healers
- 5-6 DPS
- At least one Warlock and Priest

## Thrash
- Each section of Karazhan has different thrash packs and will be mentioned in each boss guide in part 'Thrash leading to the boss'
- `Shackle Undead` and `Freezing Trap` is especially useful, Paladin's `Turn Evil` should be used with caution as it can cause pulling multiple packs and then a wipe
