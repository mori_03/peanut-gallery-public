# Moroes (Boss Encounter \#2 - Mandatory)
[[_TOC_]]

## Trash leading to boss
- Use `Shackle Undead` when needed
- Use `Frost Trap` when pulling bigger packs
- Phantom Guests (in packs of 8-10) should be AoE down instead of killing one by one due to having random abilities
- Phantom Valet have very strong melee attacks
- Phantom Attendants can heal, make sure to interrupt them
- Skeletal Waiter got a chance on each hit to debuff the attacker with `Brittle Bones` which removes all armor, tanks need to be extra careful and wait for it to wear off before pulling more
- Spectral Retainers have healing ability, Mass Dispell and Mind Controll which cannot be dispelled - nuke them first
- Clear all packs in The Banquet Hall to have enough room for the encounter


## Boss Fight
- Revolves around controlling Moroes' guests while maintaining high DPS so healers don't run out of mana because of Moroes' `Garrote`
- Use trinkets for fighting Undeads as mentioned in [Karazhan - Overview](./RaidingGuides/Karazhan/general.md)
- There's 4 adds surrounding Moroes, chosen by random out of the ones mentioned below
- If either of the adds leaves the room the fight is resetted 

## Adds Overview
- Baroness Dorothea Millstipe (shadow priest) - `Mana Burn`, `Mind Flay`, `Shadow Word: Pain`
  - Should be amongst the first to go down because of `Mana Burn`
  - Can be silenced, stunned, interrupted
- Lady Catriona Von'Indi (holy priest) - `Greater Heal`, `Power Word: Shield`, `Dispell Magic`, `Holy Fire`
  - Should be amongst the first to go down
  - `Dispell Magic` is used offensively to remove players' buffs
  - Can be silenced, stunned, interrupted
- Lady Keira Berrybuck (holy paladin) - `Holy Light`, `Cleanse`, `Divine Shield,` `Blessing of Might`
  - Primary target in case you have a priest ready with `Mass Dispell` to remove `Divine Shield`, keep her CC'd otherwise
  - `Cleanse` is usually used defensively, it can remove `Shackle Undead` or `Freezing Trap`
  - `Blessing of Might` needs to be dispelled as it gives +550 attack power, especially when used on Moroes, ideally by dps
- Baron Rafe Dreuger (retribution paladin) - `Hammer of Justice`, `Cleanse`
  - Ideally controlled by Hunter's `Freezing Trap` with caution not to get stunned
  - `Hammer of Justice` can be dispelled
  - `Cleanse` is used very rarely
- Lord Robin Daris (arms warrior) - `Mortal Strike`, `Whirlwind`
  - `Mortal Strike` hits extremely hard, which is why he needs to be CC'd or tanked
  - Melees should move out during `Whirlwind`
- Lord Crispin Ference (protection warrior) - `Disarm`, `Shield Bash`, `Shield Wall`
  - Deals very little damage but takes a long time to kill
  - Should be the very last one to kill (after Moroes)
  - Can act as a rage battery for the OT


## Encounter priorities
Due to having to deal with different combination of adds it's difficult to have a general template for the fight. However the list below should help you with understanding the priorities:
1. Healers - Lady Catriona Von'Indi (holy priest) and Lady Keira Berrybuck (holy paladin) can make encounter very long if left alive for a long time
2. Mana Burner - Baroness Dorothea Millstipe (shadow priest)
3. Stunner - Baron Rafe Dreuger (retribution paladin)
4. Warrior - Lord Robin Daris (arms warrior) - the only concern is melee dps staying out of range once he starts Whirlwind
5. Moroes
6. Survivalist -  Lord Crispin Ference (protection warrior)

**Notes**
- CC at least two adds using `Freezing Trap` and `Shackle Undead`
- Paladin can use `Turn Evil` when the mob isn't anywhere near either of the door
- Kill the other two adds first, then turn to Moroes (or keep one alive in case it's Lord Crispin Ference as a rage battery for OT)

## Moroes
- `Vanish [instant, roughly every 30 seconds]` - doesn't drop aggro
- `Garrote [instant, applied to random target shortly after Vanish, deals 1000 dmg every 3 seconds for 5 minutes]`
- `Blind [instant, causes target to become disoriented, affects Moroes' closest non-tank target]` - same as a Rogues' Blind = can be removed by Druid, Shaman or Paladin
- `Gouge [instant, stun which breaks on damage, Moroes starts attacking second highest threat target until it wears down]`
- `Enrage [instant, at 30% health]`
- `Berserk [after a certain amount if time (?) Moroes frenzies and throws daggers at random raid members]`

**Notes**
- Needs to be off-tanked because of `Gouge`
- Immune to `Taunt`
- `Garrote` is the most deadliest ability he has, it can be removed with the following spells:
  - Paladin's `Divine Shield` or `Blessing of Protection`
  - Mage's `Ice Block`
  - Hunter can use `Feign Death` to avoid it by casting the ability right when Moroes uses `Vanish`
  - Sometimes it's easier to let the target die and ressurect (`Soulstone`, `Reincarnation`, `Rebirth`)
- Have Warlock prepare their `Soulstone` and use it on target with `Garrote` debuff when called out by RL (primarily healers)
- Have Paladin cast `Blessing on Protection` on target with `Garrote` debuff when called out by RL (primarily healers)
- `Blind` needs to be soaked-up by a melee dps standing directly on top of Moroes and removed as soon as possible


## Class-specific notes

### Druid
- Non-Feral specs should be ready to remove the `Blind` as soon as possible
- `Barkskin` should be used on cooldown when affected by `Garrote` (resto/balance specs only)
- Fight can be mana intensive for the healers, don't use `Innervate` on yourself (non-resto specs), even feral tanks can do so after Moroes uses `Vanish`
- Be ready to use `Rebirth` when called out
- Make sure to use `Demoralizing Roar` on Moroes

### Hunter
- Should be able to chain-trap one add indefinitely, made even easier by Survival talents and Beast Lord Armor (2-set bonus)
- Use `Misdirection` to help off-tank maintain threat on Moroes
- Pets specced for armor and stamina can tank cloth adds during the fight (assuming they get heals)
- Keep `Scorpid Sting` on Moroes at all times
- Use `Intimidation` or `Silencing Shot` to prevent shadow priest/healer adds from casting

### Mage
- Use `Ice Block` to get rid of Garrote
- Use `Counterspell` to prevent shadow priest/healer adds from casting

### Paladin
- Use `Divine Shield` on yourself to remove `Garrote` (assuming you're not tanking)
- Use `Blessing of Protection` on targets called out by RL (generally healers
- `Turn Evil` works as a CC but don't use it when the add is close to either of the room entrances, can be used in turns with hunter's `Freezing Traps` to give him enough time to setup the next one
- Focus on removing `Blind` with `Cleanse`

### Priest
- `Shackle` Undead is your primary responsibility, as some of the adds can remove it it's safer to re-apply it every 10-15 seconds
- Shadow Priests with `Silence` can prevent shadow priest/healer adds from casting
- Make sure to use `Shadowfiend` right after Moroes comes back from stealth to maximize the mana gain


### Rogue
- Use `Kick` to prevent shadow priest/healer adds from casting
- Use `Mind-Numbing Poison` to slow down adds casting time

### Shaman
- Use `Poison Cleansing Totem` as long as Moroes is alive
- Feel free to pop `Bloodlust` on the first two adds as the fight becomes significantly easier afterwards
- When affected by `Garrote` be prepared to die and immediately `Reincarnate`
- Enhancement specs should focus on interrupting the heals and Mana Burns with `Earth Shock`
- Enhancement specs should try to stand directly on top of Moroes when attacking him to soak up `Blind`

### Warlock
- Use `Curse of Tongues` on the caster adds
- Have `Soulstone` ready to use during the fight to save potential targets and help healers conserve mana

### Warrior
- Make sure to use `Thunderclap` and `Demoralizing Shout` on Moroes
- DPS specs should use `Pummel` to prevent shadow priest/healer adds from casting
- DPS specs should try to stand directly on top of Moroes when attacking him to soak up `Blind`


## Positioning
- Keep CC'd mobs on one side while fighting on the other to avoid accidentally breaking it
- Ranged DPS should keep their distance from Moroes in case he uses `Gouge` on MT and `Blind` on OT so there's more time to remove the posion from OT
