# Shade of Aran (Boss Encounter #7 - Optional)

[[_TOC_]]

## Trash leading to the boss
- Is the same as on your way to Terestian Illhoof, see [here](./RaidingGuides/Karazhan/boss6_terestian_illhoof.md)

## Boss Fight
- Single phase fight with primary focus on positioning and avoiding his abilities
- Everybody needs to be in the same room as Aran as the door will close shortly after engaging him (?)
- Doesn't require a tank as he's got no aggro table, all his abilities attack random targets
- Very low armor, immunte to `Mind-Numbin Poision`, `Curse of Tongues` and `Silence` (Shadow Priest)
- Basic spells:
  - `Frostbolt [3 seconds]` - 3500-4500 frost damage followed by four-second (dispellable) slow
  - `Fireball [2 seconds]` - 4000-5300 fire damage
  - `Arcane Missiles [5 seconds, channeled]` - each missile hits for 1260-1540 arcane damage (6300-7700 in total)
  - `Chains of Ice [instant]` - 10-second root, doesn't break on damage, chance on hit with `Frostbolt`
  - `Counterspell [instant]` - 10yard radius, locks out schools of magic for 10 seconds
- All these spells target pets, including snakes from `Snake Trap`
- All slowing and root effects can be dispelled
- All casted abilities can be interrupted (`Kick`, `Pummel`, `Earth Shock`, `Counterspell`, `Spell Lock`)
- Interrupting is key, which abilities should be interrupted should be decided by RL based on the strategy and overall dispellable
- Casters need to stay at least 10 yards away from Aran not to get silenced
- Special abilities (used every 30-35 seconds):
  - `Flame Wreath [5 seconds]` - creates fiery rings around three random characters for 20 seconds. When the rings are triggered by moving into or out of them, they deal 3000-4000 fire damage to everybody in the room. They don't disappear upon Aran's death and can still be triggered
  - `Circular Blizzard` - large AoE that moves slowly clockwise around the room, deals 1300-1700 frost damage every two seconds and slows movement by 65%; does not hit the center of the room, if you get hit run to the center immediately
  - `Magnetic Pull + Slow + Super Arcane Explosion` - pulls everyone to the center of the room and `Slows` them, this is followed by a 10s cast of `Arcane Explosion` that deals 9-11k damage in a 20y radius
- Don't move when `Flame Wreath` appears below you, you can still heal/deal damage
- Move to the center of the room or run away from `Circular Blizzard`
- Run to the edge of the room after `Magnetic Pull + Slow`
- None of the special abilities can be interrupted
- Once Aran gets to 40% health he conjures 4 `Water Elementals` with 13k health that cast `Waterbolt` for 1000-2000 damage and despawn after 90 seconds
- `Water Elementals` need to be killed and/or controlled with `Banish`, `Fear`, or `Psychic Scream`
- Once Aran gets to 20% (or less) he casts `Mass Polymorph` onto the entire raid, conjures some water and starts regaining mana. After about 10 seconds (or if his drinking is interrupted) the polymorph breaks and Aran casts `Pyroblast` for 7000-7500 fire damage on the whole raid
- If you get a chance to interrupt his drinking early make sure everybody is at full health to survive the `Pyroblast` volley
- After 12 minutes he goes to `Berserk` mode and calls `Shadows of Aran` who have various AoE abilities and usually wipe the raid within few seconds
- Most important part beside positioning is making sure the `Mass Polymorph` and conjuring of `Water Elementals` doesn't occur close to each other as this could cause a quick wipe
- Since movement is essential in this fight healers like `Holy Priest` and `Resto Druid` are recommended due to their instant spells

## Strategy \#1 (overall low DPS)
- Interrupt only `Arcane Missiles` so Aran spends his mana quicker resulting in `Mass Polymorph + Pyroblast` happening early
- Make sure to interrupt `Arcane Missiles` as early as possible as the mana is spent immediately
- After `Mass Polymorph + Pyroblast` you can interrupt all his abilities safely to minimize damage taken

## Strategy \#2 (overall high DPS)
- Interrupt `Fireball` and `Frostbolt` but let Aran freely cast `Arcane Missiles` which means he conserves mana and `Water Elementals` occur first
- If damage output is high enough, you may avoid `Mass Polymorph + Pyroblast` completely
- Interrupt `Fireball` and `Frostbolt` halfway through the cast to give your backup interrupter enough time to react

## Class-specific notes

### Druid
- Only Resto Druids in `Tree of Life` form remain unaffected by `Mass Polymorph`, use that to your advantage to heal up and interrupt Aran's drinking (by dealing damage) when told so by RL
- Shapeshifting when standing in the fiery ring of `Flame Wreath` will trigger the AoE damage, avoid using it
- If you cast `Rebirth` and it's accepted while you're still affected with `Flame Wreath` it will trigger the AoE damage

### Hunter
- Pets can be targetted with all of Aran's abilities
- Use `Snake Trap` on top of Aran to give him extra targets for his abilities

### Mage
- Be ready to act as a backup interrupter and quickly use `Counterspell` on Aran
- Don't use `Blink` to get out of/into `Flame Wreath` as it will trigger the AoE damage

### Paladin
- Aran has no aggro table, swap `Blessing of Salvation` for something offensive
- Can use `Divine Shield` to get out of `Flame Wreath` without triggering the AoE damage
- Be ready to `Cleanse` players after `Slow` and `Chains of Ice` is used

### Priest
- Cast `Mass Dispell` after the `Magnetic Pull` to remove the upcoming `Slow`
- Use `Dispell Magic` to remove `Slow` (in case of resists) and `Chains of Ice`
- `Psychic Scream` can be used on the `Water Elementals`, using Rank 1 saves you some mana
- Aran is immune to `Silence`

### Rogue
- Focus on interrupting the abilities according to the chosen strategy
- Using `Cloak of Shadows` does not trigger the damage from `Flame Wreath`
- Aran is immune to `Mind-Numbing Poison`

### Shaman
- Your `Fire Elemental` and `Earth Elemental` crossing the ring of `Flame Wreath` will trigger the AoE damage
- Using `Reincarnation` inside `Flame Wreath` will trigger the AoE damage and most likely killing you again
- Use `Grounding Totem` to absorb Aran's basic abilities (?)
- Be ready to act as a backup interrupted and quickly use `Earth Shock` on Aran (which potentially means keeping it off cooldown as an Enhancement shaman)
- `Bloodlust` can be used right away, especially when following high DPS strategy to avoid `Mass Polymorph + Pyroblast`

### Warlock
- If you're assigned as a backup interrupted have your Fel Hunter ready to use `Spell Lock` on Aran
- Casting `Soulstone` inside `Flame Wreath` will trigger the AoE damage
- Aran is immune to `Curse of Tongues`, `Water Elementals` are not
- Control `Water Elementals` as soon as you can by `Fearing` one and `Banishing` the other

### Warrior
- Focus on interrupting the abilities according to the chosen strategy

## Positioning
- You'll likely be moving most of the fight to avoid Aran's abilities (with the exception of `Flame Wreath` during which you should stay still) so the picture below acts as an indicator of his abilities

![Shade of Aran - Positioning](aran.png "Shade of Aran - Positioning")
