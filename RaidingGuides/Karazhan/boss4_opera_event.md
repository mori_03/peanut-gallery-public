# Opera Event (Boss Encounter \#4 - Mandatory)

[[_TOC_]]

**Notes**
- Barnes (The Stage Manager) won't begin the "show" while Moroes is still alive
- Event starts after a player talks to Barnes, not before
- Encounter is chosen randomly however it stays the same for that week (is bound to your Instance ID #) in case you wipe

## Trash leading to the Boss
- First packs are the same as you encountered on your way to Maiden of Virtue
- Phantom Stagehand use immobilization and have strong melee attacks, should be `Disarmed` if possible
- Skeletal Ushers are the primary target
  - Use `Shackle Undead` if pulled in pair
  - Aggro discipline is the main concern
  - Mobs have `Frost Shock` followed by a 4 second stun, which can be `Spell Reflected`
- Spectral Performer - immune to CC, has `Sleep` ability which can be dispelled, gets dmg boost if standing in the `Spotlight`
- Other mobs shouldn't pose a threat and can be AoE'd

## Boss Fight (Randomly chosen encounter out of  the following)
- Little Red Riding Hood
    ```
    Good evening ladies and gentlemen, welcome to this evening's presentation!
    Tonight, things are not what they seem... for tonight your eyes may not be trusted!
    Take for instance this quiet elderly woman waiting for a visit from her granddaughter... surely there is nothing to fear from this sweet gray-haired old lady!
    But don't let me pull the wool over your eyes! See for yourself what lies beneath those covers! And now... on with the show!
    ```
- Romulo and Julianne
    ```
    Welcome, ladies and gentlemen, to this evening's presentation!
    Tonight... we explore a tale of forbidden love!
    But beware, for not all love stories end happily, as you may find out. Sometimes, love pricks like a thorn!
    But don't take it from me; see for yourself what tragedy lies ahead when the paths of star crossed lovers meet! And now... on with the show!
    ```
- Wizard of Oz
    ```
    Welcome, ladies and gentlemen, to this evening's presentation!
    Tonight we plumb the depths of the human soul as we join a lost, lonely girl trying desperately--with the help of her loyal companions--to find her way home!
    But she is pursued... by a wicked, malevolent crone!
    Will she survive? Will she prevail? Only time will tell. And now... on with the show!
    ```

# Little Red Riding Hood
- The Big Bad Wolf will appear on stage, has the following abilities throughout the whole fight
  - `Terrifying Howl [instant]` - causes nearby enemies to flee for 3 seconds, used every 25-35 seconds
  - `Wide Swipe [instant]` - stuns the current target for 4 seconds, used every 25-35 seconds
  - `Little Red Riding Hood [instant]` - changes random target to Little Red Riding Hood and starts chasing it. Debuff reduces target's armor and resistances to 0, increases movement speed by 50% and silences the target
- Only one tank is needed, ideally a Prot Warrior, positioned in the corner of the room
- Tank must continue generating aggro when Wolf is chasing someone or you're risking the boss going after DPS/Healer once the `Little Red Riding Hood` debuff wears off
- Ranged DPS and Healers should be in the opposite corner of the room to give them enough time to kite the boss when affected by `Little Red Riding Hood`
- If targeted by the `Little Red Riding Hood` run around the perimeter of the stage while staying close to the wall and keep away from boss' melee range, try to follow a 'square' path
- Boss is immune to most of slowing effects

## Class-specific notes

### Hunter
- Use `Feign Death` when targeted by `Little Red Riding Hood` and stay down until the debuff wears off, be ready to kite in case it resists
- Pets cannot be targeted by `Little Red Riding Hood`
- Help out with `Misdirection` to establish aggro

### Mage
- Arcane Mages can use `Slow` after boss targets someone with `Little Red Riding Hood`

### Paladin
- Using `Blessing of Protection` will prevent the player targeted with `Little Red Riding Hood` from taking damage, be sure to not use it too early to get the maximum benefit of the shield

### Priest
- Use `Power Word: Shield` on the target affected by `Little Red Riding Hood` debuff
- Use `Fear Ward` on the target affected by `Little Red Riding Hood` debuff as he might get oneshot after being feared

### Rogue
- Use `Vanish` when targetted by `Little Red Riding Hood` and stay stealthed until it wears off

### Shaman
- Make sure `Tremor Totem` is down at all times, ideally in the middle of the room
- Pop `Bloodlust` once tank has enough aggro

### Warrior
- Use `Berserker Rage` before `Terrifying Howl`
- Use `Thunderclap` to slow Wolf's damage output, especially when he starts chasing someone, as well as `Demoralizing Shout`
- Consider using `Intervene` if Wolf gets too close to the Little Red Riding Hood

## Positioning
![Big Bad Wolf Positioning](opera1.png "Big Bad Wolf - Positioning")


# Romulo and Julianne
- Encounter is split into 3 phases depending on who you fight
- Romulo uses the following abilities throughout the whole fight:
  - `Backward Lunge [instant]` - strikes at the enemy behind him, inflicting damage and Knockback
  - `Deadly Swathe [instant]` - strikes at nearby enemies in front of him (up to 3 targets)
  - `Poisoned Thrust [instant]` - attack which applies a poison reducing all stats by 10% (stacks up to 8 times)
  - `Daring [instant]` - self-buff, increases physical damage dealt by 50% and attack speed by 50% for 8 seconds
- Julianne uses the following abilities throughout the whole fight:
  - `Eternal Affection [2 seconds]` - Heals herself or Romulo by approx. 50k
  - `Powerful Attraction [1,5 second]` - Stuns the target for 6 seconds
  - `Blinding Passion [2 seconds]` - deals 4500 holy damage over 4 seconds
  - `Devotion [instant]` - self-buff, increases Holy damage dealt by 50% and casting speed by 50% for 10 seconds

## Phase \#1 - Julianne
 - Julianne appears on stage and starts attacking shortly after
 - Can by tanked by either of the tanks, even the same one who's going to be tanking Romulo in Phase #2
 - She's vulnerable to all sorts of interrupts and silences apart from Shadow Priest's `Silence`
 - Interrupters should focus on `Eternal Affection`
 - `Devotion` should be removed immediately, either dispelled or stolen
 - Let her die in a corner separe from where Romulo will be tanked to make it easier to pick her up at Phase # 3

## Phase \#2 - Romulo
 - Once Julianne dies Romulo will spawn and start attacking
 - Should be tanked by a Prot Warrior
 - Should not be tanked close to Julianne's corpse, ideally in  the other corner
 - Should be tanked with his back turned to the wall, while melee dps squeeze in behind him
 - Vulnerable to `Disarm`
 - `Daring` should be dispelled, as `Spellsteal` costs a lot of mana and isn't helpful to the mages

## Phase \#3 - Romulo and Julianne
 - Both Romulo and Julianne will ressurect and start fighting you, using the same abilities as before, so all the notes mentioned previously still apply
 - Melee should stay on Julianne for interrupts, Ranged should focus Romulo
 - Prot Warrior should be tanking Romulo because he can `Disarm` him
 - Prot Paladin/Feral Druid should be tanking Julianne
 - Both need to die within 10 seconds from each other, or the dead boss will resurrect with full heatth
 - Especially Ranged DPS should be handling the damage distribution and might have to stop dealing damage to Romulo completely

## Class-specific notes
  
### Hunter
 - Help out with `Misdirection` in the beginning of each phase
 - Can use `Arcane Shot` to remove Romulo and Julianne's self-buffs
 - Make sure to use `Scorpid Sting` on Romulo

### Mage
 - Try to `Spellsteal` Julianne's `Devotion` buff as soon as possible

### Priest
 - Can use `Dispell Magic` to remove Romulo and Julianne's self-buffs
 - Julianne's immune to Shadow Priest's `Silence`

### Rogue
 - `Kick` Julianne's abilities, primarily `Eternal Affection`

### Shaman
 - Use `Earth Shock` to interrupt Julianne, primarily `Eternal Affection`
 - Wait with `Bloodlust` until Phase #3
 - Can use `Purge` to remove Romulo and Julianne's self-buffs

### Warlock
- Julianne's immune to `Curse of Tongues`

### Warrior
 - Use `Pummel` to interrupt Julianne, primarily `Eternal Affection`
 - Use `Shield Bash` to interrupt Julianne, primarily `Eternal Affection`
 - Use `Shield Slam` to remove Romulo and Julianne's self-buffs
 - Use `Disarm` on Romulo as much as possible
 - Make sure to use `Thunderclap` and `Demoralizing Shout` on Romulo

## Positioning
- Julianne should die in one corner while Romulo in the other to make it easier in Phase 3
- Always position Romulo with his back turned to the wall

# Wizard of Oz
- Fight is split into two phases, first one revoles around controlling multiple mobs while second one is about positioning

## Phase \#1 - Dorothee, Tito, Roar, Strawman, Tinhead
- Appearing in the order mentioned above with a few seconds between each other
- Once all of them die Phase #2 begins (Tito can technically make it to Phase #2 but should be killed anyway)
- Brief overview of each add:
  - Dorothee - `Water Bolt`, `AoE Fear`, `Summon Tito` - cannot be interrupted or tanked, attacks at random
  - Tito - interrupts and silences casters, causes Dorothee to Enrage if killed before her
  - Roar - `AoE Fear` - can be feared until he's the kill target
  - Strawman - Gains `Disorient` after taking Fire damage
  - Tinhead - `Cleave` - also gets `Rust` debuff which slows him down making him easy to kite around

**Strategy**
- Dorothee {skull} is the first one to show up and is also the most dangerous, should die first and picked up by MT
- Tito {cross} can be annoying for the casters, should be killed right after Dorothee (be sure not to kill him before so Dorothee doesn't Enrage)
- Roar {square} is next, should be kept in `Fear` or `Scare Beast`, should be picked up by OT once it's his turn
- Strawman {moon} is next, should be kept `Disoriented` with by any of Mage Fire spells, Warlock's `Incinerate` or `Searing Pain` or Shaman's `Flame Shock`, can be off-tanked if needed
- Tinhead {triangle} is the last to be killed, needs to be handled by MT and faced away from the raid because of his `Cleave` and can be kited with enough stacks of `Rust` debuff

## Phase \#2 - The Crone
- After all 4 major characters have died, The Crone spawns
- Should be picked up by MT instantly
- Uses the following abilities:
  - `Cyclone [instant]` - summons Cyclone which roams around the room, dealing damage and knocking-up whoever gets hit
  - `Chain Lightning [2 seconds]` - hit's up to 5 targets for about 3000 damage
- DPS needs to spread out to avoid `Chain Lightning` from bouncing off and also avoid `Cyclone`

## Class-specific notes

### Hunter
- Help with `Misdirection` in the beginning of Phase #2
- Use `Scare Beast` to controll Roar
- Make sure your pet is not in front of Tinhead to avoid `Cleave`

### Mage
- Use Fire spells to keep Strawman `Disoriented` even Fire Wand works (as a last resort)

### Priest
- Use `Fear Ward` on tanks

### Shaman
- Can use `Fire Elemental` to control Strawman as well as `Flame Shock`
- Make sure `Tremor Totem` is used while Dorothee and Roar are alive
- Once MT has enough aggro on Dorothee pop `Bloodlust`
- Use `Grounding Totem` in Phase #2 to absorb `Chain Lightning` (?)

### Warlock
- Use `Fear` to control Roar
- Use Fire spells such as `Searing Pain` or `Incinerate` to control Strawman

### Warrior
- Use `Berskerer Rage` to get out of the AoE fears

## Positioning
- Don't stand in front of Tinhead so you don't get damaged from `Cleave`
- Spread out across the whole room during Phase #2 to minimize `Chain Lightning` damage
- Move away from `Cyclone` during Phase #2
