# Maiden of Virtue (Boss Encounter \#3 - Optional)

[[_TOC_]]

## Trash leading to the boss
- First packs are the same as you fought at Moroes' room
- After reaching Guest Quarters keep to the right to avoid pulling packs from the surrounding rooms
- Phantom Guardsman summons non-elite hounds, has `Knockdown` ability
- Spectral Sentry are hunter with `Multi-shot`
- Night Mistress - primary focus, turns to Concubine at 50% and AoE charming everyone (Shamans should use `Tremor Totems`)
- Use `Shackle Undead` if needed


## Boss Fight
- `Holy Ground [instant, permanent]` - does 240-360 holy damage every 3 seconds AoE 12 yards around her, also silences for 1 second
- `Holy Fire [1 second]` - does around 3.5k fire damage and applies a DoT
- `Repentance [0,5 second (?)]` - deals around 2k damage and incapacitates the whole raid for 12 seconds, has 30 second cooldown
- `Holy Wrath [instant]` - AoE chained holy damage with 20 second cooldown, cast on a random target
- `Berserk [instant]` - used after 10 minutes, increases damage by 500%

**Notes**
- Single phase, uses all her abilities the whole fight
- Abilities aren't always used right after they get off cooldown
- Immune to `Taunt`
- `Repentance` effect gets broken upon taking damage, which means DPS can step in the range of `Holy Ground` right before she uses it
- Enhancement Shaman, Retribution Paladin and Feral DPS should be ready to heal the MT after `Repentance` if healers aren't able to
- Tank should be ready to use defensive cooldowns, potions, healthstones to survive while healers are incapacitated
- Healers should keep HoTs on MT at all times
- PvP trinkets can be used to remove `Repentance`
- Melee DPS should be ready to heal themselves up or use potions/health-stones
- `Holy Fire` needs to be dispelled immidiately
- Shamans should use `Grounding Totems` to absorb `Holy Fire` (make sure it can reach the whole group)
- Everybody needs to spread out to avoid `Holy Wrath` from bouncing but still remain in range of dispellers


## Class-specific notes

### Druid
- Feral DPS should be ready to help with healing during Repentance if healers aren't able to

### Hunter
- `Bestial Wrath` will negate the effect of Repentance
- `Holy Wrath` cannot target/bounce off of pets, same goes for `Holy Fire`
- Maiden is immune to `Taunt`, be ready to help out with `Misdirection`

### Mage
- `Ice Block` removes the `Holy Fire` debuff
- Use `Dampen Magic` on Melee DPS (not tank)

### Paladin
- `Divine Shield` removes the `Holy Fire` debuff as well as `Repentance`
- Holy Paladin's primary concern is dispelling `Holy Fire` even at the cost of cancelling a heal
- Retribution Paladin should be ready to help out with healing during `Repentance` if healers aren't able to
- Protection Paladins can easily tank Maiden as all their abilities are instant so the brief Silence shouldn't be that big of an issue
- Be ready to use potions/health-stones when tanking during `Repentance`
- Protection/Retribution Paladins can use `Blessing of Sacrifice` to break free of `Repentance`

### Priest
- Holy Priest's primary concern is dispelling `Holy Fire` even at the cost of cancelling heal, Shadow Priest should be ready to help out as well
- If you time `Shadow Word: Death` correctly you can get out of `Repentance` instantly as it returns damage onto the caster

###  Rogue
- `Cloak of Shadows` removes the `Holy Fire` debuff

### Shaman
- Enhancement Shaman should be ready to help out with healing during `Repentance` if healers aren't able to
- Use `Bloodlust` right after `Repentance` effect wears off for its maximum efficiency
- Use `Grounding Totem` so it covers your entire group to help soak up the damage from `Holy Fire`

### Warrior
- `Berserker Rage` makes you immune to `Repentance`
- If assigned to tanking Maiden and in a risk of dying due to not receiving heals during `Repentance` you may quickly use `Intervene` on one of the healers to bring the boss to them and break the `Repentance` effect, only use as last resort

## Positioning
- Everybody needs to spread out to avoid `Holy Wrath` from bouncing off
- Everybody needs to be close enough to be dispelled by Priest or Paladin
- Melee should stand on the edge of her hitbox and form a triangle/square around Maiden
- When `Repentance` is off cooldown ranged DPS/Healers should make a step forward so they get hit by `Holy Ground` and break the Concubine

![Maiden of Virtue - Positioning](maiden.PNG "Maiden of Virtue - Positioning")
