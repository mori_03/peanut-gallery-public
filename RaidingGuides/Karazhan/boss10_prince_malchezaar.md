# Prince Malchezaar (Boss Encounter #10)

[[_TOC_]]

## Trash leading to the boss
- There's only a few packs between Gamesman's Hall and Netherspace (where Prince Malchezaar is) but they should be pulled with caution and in the smallest possible pulls
- Flashbeasts have strong bleed effect, damage dealing disease debuff and painful frontal cleave, if pulled in pairs tanks should stay a few yards from each other and DPS should focus one of these at a time
- Greater Flashbeast have decent AoE damage within 10 yards, melee should approach with caution and be ready to step away to get heals/bandage themselves

## Boss Fight
- Fight has somewhat random nature as it depends on the positioning of Infernals that are being summoned throughout the fight
- The longer the fight the less room you'll have, so the higher the overall DPS is the easier the fight will be
- Fight can be split into 3 phases based on Prince's health

### Boss abilities
- `Summon Infernal [instant]` - lands at a random spot on the platform, starts casting `Hellfire` after 3 seconds, despawns after 180 seconds; used in all phases
- `Enfeeble [instant]` - affects 5 random targets (excluding the highest target in aggro table), reduces health to a single HP for about 7 seconds, during this time healing received is reduced by 100%, players targeted by this should move away and wait for `Shadow Nova`; used in Phase #1 and #2
- `Shadow Nova [2 seconds]` - deals about 3000 shadow damage in 24y range and knocks back, used ~4 seconds after `Enfeeble` during Phase #1 and #2, used without preliminaries in Phase #3
- `Shadow Word [instant]` - casts a shadow DoT (just like Priest' `Shadow Word: Pain`) on his primary target dealing 1500 damage each tick, cannot be reflectd, can be dispelled; used in Phase #1 and Phase #3
- `Thrash [instant]` - gives to extra rapid-fire melee attacks agains the primary target, used randomly; only in Phase #2
- `Parry [instant]` - avoid a frontal melee attack, which means the next attack is 40% faster, especially dangerous in combination with `Thrash`; only used in Phase #2
- `Sunder Armor [instant]` - used on his primary target causing armor reduction; only used in Phase #2
- `Flying Axes [instant]` - Malchezaar unleashes his axes on random targets as separate but untargetable entity, Axes deal physical damage starting for only about 400 on cloth while ramping it up to 2000 a hit; only used in Phase #3
- `Amplify Damage [instant]` - debuff on players causing them to take 200% damage from all sources, cast on random target and isn't dispellabale; ony used in Phase #3

### Phase \#1 (100 - 60% health)
- Tank should have his back against the wall to avoid knockback from `Shadow Nova`
- DPS need to go easy with aggro as he's immune to `Taunt`
- Ranged/Healers should stay ~30 yards away from the boss
- When `Enfeeble` is used all melee should run out towards the healers so they don't get hit by `Shadow Nova`
- Watch out for `Infernals` and re-position accordingly
- Dispell `Shadow Word` from the tank

### Phase \#2 (60 - 30% health)
- Warps his axes which increases his attack damage considerably
- Tank should have his back against the wall to avoid knockback from `Shadow Nova`
- Tank should be ready to use his defensive cooldowns, potions and healthstones as `Thrash` can deal over 12k damage within a seconds
- Healers should have tank topped off most of the time
- Apply as much damage reducing abilities as you can to minimize Prince's heavy physical damage
- When `Enfeeble` is used all melee should run out towards the healers so they don't get hit by `Shadow Nova`
- Watch out for `Infernals` and re-position accordingly
- DPS should use all offensive cooldowns to get through this phase as quickly as possible

### Phase \#3 (30 - 0% health)
- Tank should have his back against the wall to avoid knockback from `Shadow Nova`
- `Enfeeble`, `Thrash`, `Parry`, `Sunder Armor` is no longer used
- `Shadow Nova` is used randomly, so melee might have a hard time dealing damage due to knockback (should be ready to bandage themselves)
- Healing should be focused on the person targeted by `Flying Axes`, especially if he gets `Amplify Damage` debuff at the same time
- `Summon Infernal` is used more frequently
- Watch out for `Infernals` and re-position accordingly
- Killing him quickly (approx. within 2 minutes) is essential as you might run out of room due to `Infernal` positioning
- Dispell `Shadow Word` from the tank

## Class-specific

### Druid
- Use `Barkskin` when targetted by `Flying Axes` if possible
- Consider using `Elixir of Demonslaying` (Feral spec)

### Hunter
- Help out with `Misdirection` as much as you can as Malchezaar is immune to `Taunt`
- Use `Track Demons` to spot the `Infernals` quickly in case the raid needs to reposition
- Be prepared to use `Feign Death` to avoid over-aggroing while still maintaining high DPS
- Pets cannot get hit by `Enfeeble`
- Consider using `Elixir of Demonslaying`

### Paladin
- Recommended tank for the fight as `Holy Shield` has more charges than `Shield Block` and can enable him remain uncrushable even during Phase #2
- Retribution Paladins must avoid using `Seal of Blood` during Phase #1 and #2
- When tanking during Phase #2 you can stop melee attacks and focus on dealing magic damage to reduce the chance of `Parry` damage spikes
- Switch to `Devotion Aura` in Phase #3 to minimize the damage from `Flying Axes` if you don't already have another paladin in a group using it
- Focus on dispelling `Shadow Word` during Phase #1 and #3
- Can use `Divine Shield` when targetted by `Flying Axes` if healers are running low on mana, same for `Blessing of Protection`
- Consider using `Elixir of Demonslaying` (Retribution spec)

### Priest
- Focus on dispelling `Shadow Word` during Phase #1 and #3
- Avoid using `Shadow Word: Death` during Phase #1 and #2
- Buff everyone with `Prayer of Shadow Protection`
- Use `Power Word: Shield` on `Flying Axes` targets

### Rogue
- Use `Vanish` to avoid over-aggroing while still maintaining high DPS
- Consider using `Elixir of Demonslaying`

### Shaman
- Tank should have enough aggro at the end of Phase #1, use `Bloodlust` once Malchezaar gets to 60% health
- Consider using `Elixir of Demonslaying` (Enhancement spec)

### Warlock
- Be ready to use `Curse of Weakness` during Phase #2 if the tank is taking too much damage
- Be prepared to use `Soulshatter` to avoid over-aggroing while still maintaining high DPS
- Use `Sense Demons` to spot the `Infernals` quickly in case the raid needs to reposition

### Warrior
- Consider using `Elixir of Demonslaying`
- Make sure `Thunderclap` and `Demoralizing Shout` is applied during Phase #2 to help minimize damage

## Positioning
- Tank should have his back against the wall throughout the fight to avoid knockback
- Melee DPS need to move away once `Enfeeble` hits as it's followed by `Shadow Nova`
- Ranged should keep ~30 yards away from boss
- Watch out for `Infernals` and be ready to reposition
- If you're running out of room, go into range of `Shadow Nova` rather than getting hit by `Hellfire`
