# Chess Event (Boss Encounter #9 - Mandatory)
[[_TOC_]]

## Trash leading to the boss
- There's only 3 packs between Netherspite's room and Gamesman's Hall which shouldn't pose big of a threat

## Boss fight
- Non-traditional encounter which requires no consumables or using any player abilities, simple a chess game against Medivh on an 8x8 chess board
- Start the game by talking to the king of your own faction (Warchief Blackhand)
- Game is won once you kill the opposing king (King Llane), after that a Dust Covered chest will appear containing the loot
- It's possible to reset the event by talking to Echo of Medivh
- Once the event begins, raid members can control any currently uncontrolled piece if their of faction by talking to it, upon talking control of a piece the player's character is teleported onto a platform at the side of the chess field, gains `Control Piece` buff, his viewing perspective changes to that of the controlled piece and their action bar is replaced
- You may release control of a piece by remove the said buff (right-click it) which gives you a 15 second debuff making you unable to control another pieces
- Each piece has 4 abilities:
  - `Move` - click on an unoccupied square, only certain moves are allowed
  - `Rotate Piece` - click an adjacent square on the board to face toward it
  - `Special Ability 1, Special Ability 2`
- `Move` and `Rotate Piece` share a 5 second cooldown, as well as both special abilities

## Piece overview

| Piece  | NPC Name (Alliance) | NPC Name (Horde)   | Movement              | Special Ability #1       | Special Ability #2         | Notes                        |
| ------ | ------------------- | ------------------ | --------------------- | ------------------------ | -------------------------- | ---------------------------- |
| King   | King Llane          | Warchief Blackhand | 1 space any dir.      | Sweep/Cleave             | Heroism/Bloodlust          | High HP, High DMG            |
| Queen  | Human Conjurer      | Orc Warlock        | 3 straight/2 diagonal | Elemental Blast/Fireball | Rain of Fire/Posion Cloud  | Medium HP, High DMG          |
| Bishop | Human Cleric        | Orc Necrolyte      | 1 space any dir.      | Healing/Shadow Mend      | Holy Lance/Shadow Spear    | Low HP, Low DMG, only healer |
| Knight | Human Charger       | Orc Wolf           | 2 straight/1 diagonal | Smash/Bite               | Stomp/Howl                 | Medium HP, Low DMG           |
| Rook   | Water Elemental     | Summoned Deamon    | 1 space any dir.      | Geyser/Hellfire          | Water Shield/Fire Shield   | Low HP, Medium DMG           |
| Pawn   | Human Footsman      | Orc Grunt          | 1 space any dir.      | Heroic/Vicious Strike    | Shield Block/Deflection    | Low HP, Low DMG              |

## Medivh's cheats
- Happens at random interval, usually only prolongs the whole event
- `Healing Cheat` - Heals Medivh's king and possibly other pieces to full health
- `Damage Cheat` - Medivh places AoE fire on a random player controlled piece, simply the piece away
- `Berserking Cheat` - One of Medivh's unit gains increased size, speed and damage

## Strategy
- Simplest strategy is to kill the bishops, queen and finally the king
- Your bishops should only move to avoid Medivh's AoE and focus on healing the king and queen
- Move your Pawns to make enough room for the "better" pieces
- Once Pawns cleared the board take control of other pieces and focus on killing Medivh's critical pieces
- Use king offensively as he's got the highest damage
