# The Curator (Boss Encounter \#5 - Mandatory)

[[_TOC_]]

## Trash leading to the boss
- First packs you'll face are similar as those you encountered coming towards the Opera House
- Focus Trapped Souls on The Broken Stairs due to their `Cone of Cold` ability and be sure to remove their `Elemental Armor` buff
- In The Menagerie (where you're going to fight Curator) you'll meet three kinds of mobs
  - Arcane Watchman - uses `Overload` which causes one party member to periodically explode into an `Arcane Explosion` damaging everyone around, damage encreases with each explosion, affected player needs to move away from the raid
  - Arcane Anomaly - should be the primary target since they regen mana after dying which helps counter Syphoners mana drain, start by `Misdirect` those onto the Prot Warrior/Feral Druid with `Aimed Shot`
  - Syphoner - mana drainers (dispellable), they should be AoE'd down and tanked away from healers if possible

## Boss fight
- Encounter consists of 3 phases, two of which alternate until 15%, when the third one begins
- Required two tanks
- Curator uses the following abilities:
  - `Summon Astral Flare [instant (?)]` - Summons Astral Flare, used every 10 seconds in Phase #1; Astral Flare has 17k health, is immune to `Taunt` and has chained attack which hits up to 3 target within 10 yards dealing approx. 700 Arcane damage
  - `Hateful Bolt [1 second]` - Hits non-tank character dealing 4500-6000 Arcane damage
  - `Evocation [20 seconds, channeled] ` - Curator goes inactive for 20 seconds while re-filling mana and increasing damage taken by 200% (Phase #2)
  - `Soft Enrage` - At 15% health, increase melee attack speed and `Hateful Bolt` cast rate while ceasing new `Evocations` and `Astral Flare Summons`
  - `Hard Enrage` - After 10 minutes
- Immune to Arcane, Poision, Taunt and Mana Draining abilities
- Most of the fight consists two alternating parts: Phase #1 (90 seconds) followed by Phase #2 (20 seconds), right up until Curator is down to 15% health when Phase #3 starts

### Phase \#1 (Flare Phase)
- Lasts for 90 seconds before switching to Phase #2
- Curator summons `Astral Flare` every 10 seconds and casts `Hateful Bolts`
- Focusing down `Astral Flares` is the main priority for all DPS
- Each `Astral Flare` should be killed in 10 seconds (has 17,000 health) before the next one is summoned
- `Astral Flare` don't need to be tanked but it's easier if they are (Pet with high stamina, DPS Warrior, ...)
- It's not necessary to damage Curator at all during this phase, only when no `Astral Flares` are up
- `Hateful Bolt` is used every 10-15 seconds and should target the second highest threat target, which is why OT needs to keep aggroing for the whole fight
- Each `Summon Astral Flare` costs Curator 10% mana, once he summons the 10th one damage it down quickly and get to Curator immediately

### Phase \#2 (Evocation Phase)
- After Curator runs out of mana by summoning the 10th `Astral Flare` he starts casting `Evocation`
- Quickly destroy the last `Astral Flare` and deal as much damage to the boss as possible
- Aggro shouldn't be an issue since tanks are getting aggro in Phase #1
- Use all possible offensive cooldowns to deal as much damage
- Apart from the leftover damage from the last `Astral Flare` no healing is required, healers can deal damage as well if they're not in a risk of running out of mana

### Phase \#3 (Soft Enrage)
- Begins once Curator gets to 15% health
- Boss no longer uses `Summon Astral Flare` and `Evocation`
- `Hateful Bolt` is used more frequently
- His melee attacks deal more damage, healers should only focus on tanks

## Class-specific notes

### Druid
- Save `Innervate` for healers as the fight can be very mana intensive
- Curator is immune to Arcane damage, Balance druis should only use `Wrath` and `Insect Swarm`

### Hunter
- Help out with `Misdirection` if necessary (MT or OT, depends on the situation)
- If your pet has high stamina and resistances try to let him tank the `Astral Flares`
- Use offensive cooldowns such as `Bestial Wrath` and `Rapid Fire` during Phase #2
- Curator is immune to Arcane damage, don't use `Arcane Shot`
- Curator is immune to Poisions as well, don't use any `.* Sting` abilities

### Mage
- Curator is immunte to Arcane damage

### Paladin
- Aggro shouldn't be a problem, consider swapping `Blessing of Salvation` for some DPS increasing buffs
- Use `Avenging Wrath` during Phase #2

### Priest
- Using `Prayer of Mending` is highly recommended during Phase #1, same goes for `Circle of Healing` (requires solid gear to be efficient)
- Use `Shadowfiend` during Phase #2

### Rogue
- Curator is immune to Poisions, consider swapping to Sharpening Stones for the encounter

### Shaman
- Use `Bloodlust` when dealing with the 8th `Astral Flare` to quickly transition to Phase 2

### Warlock
- Use `Curse of Doom` on Curator when dealing with 4th Flare
- Once `Curse of Doom` goes off during Phase #2, replace it with standard curses `of Elements, Shadow, Recklesness`

## Positioning
- Curator is usually positioned in the middle of the room with his back turned to the raid (so rogues/feral dps don't have to run as far)
- Ranged DPS and healer should spread out to minimize `Astral Flare` damage
- `Astral Flares` spawn to the left or right of Curator in pattern, melees should look out for that to damage them right away
