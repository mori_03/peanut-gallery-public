- [Preparing for raiding](#preparing-for-raiding)
  - [TALENTS AND PRE-RAID BIS](#talents-and-pre-raid-bis)
  - [KARAZHAN ATTUNEMENT](#karazhan-attunement)
  - [RAIDING EXPECTATIONS](#raiding-expectations)
    - [Required Consumables](#required-consumables)
    - [Required Addons](#required-addons)

# Preparing for raiding
Since TBC is at our doorstep, we thought we’d emphasize a little bit what is expected of our raiders. As has been clear since the beginning, we are not a sweaty hardcore raiding guild but we will still be expecting a certain level of dedication to the raid. And so we have put together just a quick little guide on the few things we expect everyone to have prepared.

## TALENTS AND PRE-RAID BIS
First off, while Teakay is expecting a child, he’s also expecting your speccing to be spectacular. *Respec it!*

Research and figure out which is the ultimate PvE-spec for your class and spend your beautiful talent points accordingly. If you for any reason are uncertain, insecure or too dumb to figure out which one – we always encourage for you to reach out to one of the other raiders of your class, an officer, or discuss it in `#class-discussion`. Valuable sources for research could your respective class discord, wowhead and [seventyupgrades](https://seventyupgrades.com/). The world is your oyster. Make oyster sauce (because it’s necessary for a great peanutbutter sauce).

Second off, while Teakay is busy dreaming about playing frisbees with his babies he would feel bliss, if you got your BiS business in order. So start by finding one, two or three guides out there on your pre-raid BiS. Preferably get a good understanding of what these items will be before TBC launch, so you have it in mind while leveling. 

1. So, to state it emphatically, we do not require you to choose the ideal profession for your class. You are free to choose any profession you want, but we of course always recommend you choose one that leads to that sweet pre-raid BIS item. But if you want to pick and skin peanuts for the cheddar, then you do you.
2. We strongly advocate you level up at least a little bit in dungeons to boost that reputation so you can run heroics as peanut as possible. Figure out which heroics it is that offer your respective pre-raid BIS items and make an effort to gain some reputation while leveling so that when you hit 70 you do not need to grind those instances purely for rep later on. 
3. Research and choose your faction, `Aldor` or `Scryer`, that is most suited for your PvE-oriented gamestyle. You get to choose which faction you want to adhere to on level 60 already so we suggest you figure it out before TBC launch which one you are going for!
4. Research and determine whether there are any of the quest rewards that you gain while leveling which are pre-raid BIS, so that you do not end up choosing an item that might be good for your leveling specc where an alternative quest reward would be pre-raid BIS for your future raiding spec.

Here’s an example from Teakay:
Pre-raid BIS: https://seventyupgrades.com/set/gxbFFLYKUKEFJhHR2gY2Vr
Tier 4: https://seventyupgrades.com/set/uYT4HSNu45yhNknorvno2a

## KARAZHAN ATTUNEMENT
0. First, you need to do a little escort quest in the `Caverns of Time` zone in `Tanaris`. Now, since we are playing on a heavily populated server, this escort quest might turn quite tricky to finish (since you’ll likely be ganked while trying to complete the escort quest). A trick here is, in case you do get ganked, is that you can return to the corpse and wait until the escort npc is about to return back to where the quest started – and if you resurrect slightly before, you’ll get the completion of the quest.
1. We recommend you do `Escape for Durnholde Keep` at 66-67 already, it will be necessary to have it completed before being able to enter `CoT: The Black Morass` (step 5).
2. Since we want to start running at least one group of Karazhan as soon as possible, make sure you start the attunement chain as soon as it is available on level 68. It starts with two quests from Archmage Alturus outside of `The Violet Eye: Arcane disturbances` and `Restless activity`. After these two starting quests you’ll do some classic running to Dalaran and then to Shattrath City.
3. Next up is `Entry into Karazhan` which needs you to go into `Shadow Lab` in Auchindoun, to get the `Master’s Key`. It lies in a container close to Murmur, which is the final boss. Return to Khadgar.
4. After this it’s The Second and Third Fragments, these items comes from `Steamvault` (second) and `Arcatraz` (third). Return to Khadgar.
5. The final step is for you to go to C`averns of Time: The Black Morass`. There you speak to Medivh in the beginning and after the completion of the instance he’ll turn the quest item key to a Master’s Key. Return to Khadgar.
6. Go to Deadwind Pass. Voila, you are now a legit peanut.

## RAIDING EXPECTATIONS
Appropriate for any dignified peanut is to come prepared to a raid. We don’t have sweaty requirements, but we do have some. 
1. You make sure you are attuned to the instance that you’re in a raid for and that you are there on time. **That means you’re there latest at 18:45 – we’re pulling at 19:00**
2. You are specced for optimal PvE-performance. 
3. You have read [Nihi’s boss-sheets](https://gitlab.com/mori_03/peanut-gallery-public/-/blob/master/README.md) prior to the run, and you have them easily accessible before the raid. We will be running the strategies in these guides and not other viable ones. Have them on a second monitor, window for alt+tabbing, printed out, etched into a large stone tablet or written in peanutbutter on your wall – just have them ready.
4. You have the consumables to last you four hours of raiding, with potential wipes accounted for – the mandatory amount of consumables should therefore last you for five hours. You can find a list of consumables here

### Required Consumables
* `Flask or Battle & Guardian Elixirs` to last you the whole raid evening
* `Health/Mana/Other Potions` - even if you're not likely to use them, have them in your bags in case of emergency
* `Buff food`
* `Bandages` - First Aid isn't mandatory but highly recommended, useful during long fights and after wipe so the raid can get moving faster

### Required Addons
Every raider/trial in Peanut Gallery must install the following Addons:
* `a threat meter` Omen, DTM, whatever's popular and working. Examples:
  * [ThreatClassic2](https://www.curseforge.com/wow/addons/threatclassic2)
  * [Details! Damage Meter (TBC Version)](https://www.curseforge.com/wow/addons/details-damage-meter-burning-crusade)
* [CEPGP](https://www.curseforge.com/wow/addons/cepgp) (you don't have this you don't get loot)
* [Attune](https://www.curseforge.com/wow/addons/attune) (for keeping track of attunements guildwide)
* `Something like DBM/BigWigs` whatever you like better
  * [Deadly Boss Mods](https://www.curseforge.com/wow/addons/deadly-boss-mods)
  * [BigWigs](https://www.curseforge.com/wow/addons/big-wigs)

Additionally, healers must have the following installed:
* `Proper raid unit frames` like for example:
  * [Grid2](https://www.curseforge.com/wow/addons/grid2)
  * [Healbot Classic TBC](https://www.curseforge.com/wow/addons/healbot-classic-tbc)
* [Decursive](https://www.curseforge.com/wow/addons/decursive) (if your raid unit frames doesn't have such functionality)

All Paladins MUST have the following addons installed:
* [PallyPower](https://www.curseforge.com/wow/addons/pally-power) - for organizing blessings

> NOTE: Either `@officer` or `@guild master` will announce that content is on farm.

