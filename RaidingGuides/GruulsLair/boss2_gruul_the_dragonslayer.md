# Gruul the Dragonslayer (Boss Encounter \#2)

[[_TOC_]]

Encounter is fairly straight-forward and is considered the first actual raid DPS check. Throughout the whole fight Gruul uses the following abilities:
- `Growth [instant, cannot be dispelled]` - self-buff, increases damage done by 15% and size by 10% and is used every 30 seconds
- `Hurtful Strike [instant]` - always hits the second highest aggro target within melee range, performed roughly every 20 seconds
- `Reverberation [instant, zone-wide]` - 4 second lasting silence, cannot be dispelled
- `Cave In [instant]` - Random AoE which causes 3000 damage every 3 seconds in the area, the AoE damage is preceded by ground shake in the affected area and is also visible on the ceiling. Occurs more frequently as the fight progresses and the damage scales with `Growth`
- `Ground Slam [instant]` - zone-wide knockback into a random direction which is followed by `Gronn Lord's Grasp`
- `Gronn Lord's Grasp [instant]` - stacking debuff after Ground Slam that reduces movement speed by 20%, stacks 5 times, then causes Stoned effect after 5 seconds
- `Stoned [instant]` - Stun, occurs after 5 stacks of `Gronn Lord's Grasp`
- `Shatter [instant]` - after being `Stoned`, Grull will `Shatter` everybody, the damage dealt increases if players are close to each other. Damage is physical and goes through immunity shields. Ranges from 1100 if just within 15+ yards to over 8500 if standing very close. Pets do not cause damage or get damage with this ability but can be stunned.


**Notes**
- Gruul needs to be killed before 16 stacks of `Growth` as he could one-shot OT after
- Should be tanked in the middle of the room where he stands, OT needs to be second on threat at all times to soak up `Hurtful Strike` damage
- `Hurtful Strike` cannot hit ranged dps but they should keep their threat below OT to avoid any possible confusion
- `Cave Ins` frequently spawn on top of Gruul himself which means melees need to even more alert to it
- First damage of `Cave In` is dealt after 3 seconds, quick-footed players can avoid it completely
- Positioning during `Shatter` is vital as too many deaths lead to low DPS, prioritize it over doing damage
- When engaged everybody needs to be in the big room as the door will close when you enter combat
- Ranged DPS/Healers should spread out across the whole cave, if possible with their back against the wall to avoid knockback
- Healers main priority is to make sure OT gets topped off immediately after `Hurtful Strike` and also making sure MT survives `Reverberation` by stacking HoTs on him shortly before and making sure he's at full HP at all times

## Class-specific notes

### Druid
- Use `Barkskin` before every Shatter to minimize damage taken
- Feral Druids are perfect off-tanks in this fight as `Hurtful Strike` cannot crit and cannot be a crushing blow so all that is needed is armor, health and threat generation
- Shapeshifting works on the slow debuff of `Gronn Lord's Grasp` but will reappear after 1-2 seconds
- As the fight can be mana intensive for healers non-resto specs should save `Innervate` for the healers

### Hunter
- Should be positioning themselves behind the obstacles and at maximum range to make room for the rest of the raid due to their long attack range
- Pets cannot be affected by `Hurtful Strike` or `Shatter`, but do receive the `Stoned` stun
- Pets will not be specifically targeted by `Cave In` but are vulnerable to its damage
- Make sure to keep `Scorpid Sting` on Gruul at all times
- Help out by using `Misdirection` on the OT to make sure he stays second in threat table at all times

### Mage
- May use `Ice Block` to remove the movement speed debuff but it does NOT prevent any damage being done by `Shatter`
- Buff the whole raid with `Amplify Magic` as Gruul only deals physical damage

### Paladin
- May use `Divine Shield` to remove the movement speed debuff but it does NOT prevent any damage being done by Shatter
- `Blessing of Protection` doesn't work on `Gronn Lord's Grasp/Stoned`

### Rogue
- Can use `Cloak of Shadows` to remove the movement speed debuff but it does NOT prevent any damage being done by `Shatter`

### Shaman
- `Bloodlust` can be used once both tanks have enough threat and nobody is in a risk of over-aggroing
- Resto Shamans with `Focused Mind` talent (Reduced duration of Silence or Interrupt by 10/20/30%) should be ready to quickly top-off the main tank after `Reverberation`, if necessary even in combination with `Nature's Swiftness`

### Warrior
- In case of a 3rd tank being present for Maulgar fight he can use `Intervene` on the main tank after the silence to soak up some damage

## Positioning

![Gruul - Positioning](gruul.PNG "Gruul - Positioning")
