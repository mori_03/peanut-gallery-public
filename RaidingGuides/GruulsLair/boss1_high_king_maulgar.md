# High King Maulgar (Boss Encounter #1)
[[_TOC_]]

## Adds overview
- Blindeye the Seer (priest)
- Olm the Summoner (warlock)
- Krosh Firehand (mage)
- Kiggler the Crazed (shaman/mage)

### Blindeye the Seer {skull}
- `Power Word: Shield [instant]`
- `Prayer of Healing [2 seconds]`
- `Prayer of Mending [10 seconds]`

**Notes**
- Primary target, should die very quickly
- Tanked by an off-tank, ideally warrior in some DPS gear to avoid rage starvation
- Positioned next to Olm so he can be cleaved and dps can quickly swap to Olm after Blindeye dies
- `Prayer of Healing` can be stopped by any interrupt
- `Power Word: Shield` is always followed by `Prayer of Mending`, you can either burst through the shield and then interrupt the cast in any way or use one of the following abilities to interrupt the cast while shield is still active:
  - `Silence` (Shadow Priest)
  - `Improved Kick` (Combat Rogue)
  - `Silencing Shot` (Marksmanship Hunter)
  - `Intimidiation` (Beast Mastery Hunter)
  - `Improved Shield Bash` (Protection Warrior)
  - `Intercept` (Any Warrior)
  - `Improved Counterspell` (Arcane/Frost Mage)
- Have a rogue use `Mind-Numbing Potion` to prolong casting time together, or a warlock with `Curse of Tongues`
- Apply healing debuff - either Arms Warrior's `Mortal Strike` or Hunter's `Aimed Shot` to minimize healing


### Olm the Summoner {cross} 
- `Dark Decay [instant DoT, stackable]`
- `Death Coil [instant]`
- `Summon Wild Fel Stalker [instant]`

**Notes**
- Secondary target, should be tanked next to Blindeye
- Tanked by either two Enslaved Fel Stalkers or an Enslaved Fel Stalker and off-tank (any will do) in case the main tank gets hit by `Death Coil`
- Stacking DoT `Dark Decay` can only be cleansed by the enslaved Fel Stalker (short CD, not on auto-cast)
- Any additional Fel Stalkers need to `Banished` or `Feared`
- Dispose of excess Fel Stalkers by letting it use `Taunt` on Krosh {square}, same goes for the Fel Stalkers used for tanking once Olm dies


### Krosh Firehand {square}
- `Spell Shield [instant, decreases magic dmg taken by 75%, 30 second cooldown]`
- `Greater Fireball [3 seconds]`
- `Blast Wave [instant, 15 yard radius]`

**Notes**
- Tertiary target, tanked by a mage with high stamina gear (10.5k health unbuffed or more)
- Tanked away from other because of his `Blast Wave` ability
- Mage assigned to tank him needs to focus on using `Spellsteal` right after Krosh casts `Spell Shield` onto himself
- Melee shouldn't go anywhere near him, needs to be killed by Ranged DPS
- Hunters **MUSTN'T** use `Arcane Shot` not to remove Spell Shield before Mage has a chance to steal it
- Krosh's `Greater Fireballs` ignore Fire Resistance, Grounding Totem and Krosh himself is immune to `Curse of Tongues`
- Elemental Shaman should place `Totem of Wrath` and `Mana Spring Totem` near the tanking mage as he usually has to sacrifice some spell hit for stamina


### Kiggler the Crazed {moon}
- `Greater Polymorph [instant, cannot be dispelled]`
- `Lightning Bolt [2 seconds]`
- `Arcane Explosion [instant]`
- `Arcane Shock [instant]`

**Notes**
- Last add to be killed before Maulgar himself
- Tanked by a Moonkin (due to his immunity to Polymorphs) or two ranged DPS (ideally Hunters because Silence doesn't affect them)
- `Lightning Bolts` deal minor damage, can be absorbed with Grounding Totems
- `Arcane Explosion` deals minor damage but drops threat and knocks back all targets within 30 yards which is why he's tanked away from others
- `Arcane Shock` deals minor damage but silences and disarms the target and is always cast on the target with highest threat
- `Greater Polymorph` cannot be dispelled
- Nature Resistance gear can be beneficial but shouldn't be necessary (`Nature Resistance Totem`/`Aspect of the Wild`) should be enough
- Due to his low damage melee can get close to him assuming they position themselves to negate the knockback otherwise they might end up in Maulgar's `Whirlwind`, can even stand in front of him as he's got no cleave-like abilities


## High King Maulgar {triangle}
- `Arcing Smash [instant cleave]`
- `Mighty Blow [instant knock-back]`
- `Whirlwind [15 seconds channeling, 25-35 seconds cooldown]`
- `Charge [instant, only in Phase #2]`
- `Intimidating Roar [instant AoE fear which also stuns the tank, 15 second cooldown only in Phase #2]`

### Phase \#1 (100 - 50% health)
- Final mob, should be killed once all his adds are already dead
- Tanked by a regular main tank (ideally Feral Druid)
- Tank should be with his bank against the wall to minimize knock-back from `Mighty Blow`
- `Demoralizing Shout/Roar` and `Thunderclap` are very helpful
- Everybody except for the tank should move out once he starts channeling `Whirlwind`
- Melees need to be attacking from behind to avoid damage from `Arcing Smash`

### Phase \#2 (50 - 0% health)
- Everything mentioned above still applies for Phase #2, Maulgar gets two extra abilities
- Maulgar drops his weapon (reducing his attack damage by a great deal) and gains `Enlarge`
- Shamans should drop `Tremor Totem` shortly before Maulgar gets to 50%, Priests should use `Fear Ward` on off-tank
- Once Maulgar uses `Intimidating Roar` everybody in range gets feared and tank gets stunned, which means off-tank should taunt it immediately (hence the `Fear Ward`), MT will taunt back once he can (stun should last 4 seconds)
- After `Charge` there's a slight drop of aggro on the top threat target (?)
- `Whirlwind` can be deadly if used right after `Charge`/`Intimidating Roar` so all ranged dps and healers need to spread around him to minimaze damage

## Suggested raid setup
- 4 Hunters (`Misdirection` priority: Krosh > Maulgar > Olm > Kiggler > Blindeye)
- 1 Mage tank (At least 10.5k health unbuffed, ideally with an elemental shaman in group)
- 2 Warlocks for Olm
- 2 Ranged DPS/1 Balance Druid to tank Kiggler
- 1 Tank on Maulgar (ideally a Feral Druid due to high armor)
- 1 Warrior on Blindeye
- 1 Tank on roaming Fel Stalkers (in case of resist from a warlock)
- 2 Rogues 

## Class-specific notes

### Druid
- Balance Druid tanking should get some Nature Resistance via buffs (`Gift of the Wild`, `Aspect of the Wild`, `Nature Resistance Totem`)

### Hunter
- Hunters tanking should get some Nature Resistance via buffs (`Gift of the Wild`, `Aspect of the Wild`, `Nature Resistance Totem`)
- Ideally have one hunter for each add and Maulgaur to help with threat in positioning during the pull
- Use `Aimed Shot` on Blindeye the Seer to minimize his healing done
- Don't use `Arcane Shot` on Krosh Firehand not to accidentally dispell `Spell Shield`
- Marksmanship Hunters can use `Silencing Shot` on Blindeye even when he has shielded himself, same goes for Beast Mastery hunters with `Intimidation`
- Be careful about pet positioning as they can die to `Whirlwinds`/`Blast Wave` fairly quickly
- Keep `Scorpid Sting` active on Maulgar at all times
- Keep `Aimed Shot` debuff on Blindeye at all times
- Use `Aspect of the Wild` when tanking Kiggler

### Mage
- The one tanking Krosh needs to stack high stamina gear, if possible get some extra spell hit as well
- Spellstealing `Spell Shield` is your primary concern as you're most likely dead without it
- Start the pull by doing an instant ability (`Presence of Mind` + `Pyroblast` or `Fireblast`) then use `Spellsteal` and then start gaining some threat
- Krosh will only follow you to where you are when you're doing the pull, he won't move any furher afterwards
- Krosh has no ability to drop your aggro so once you gather enough you can consider not casting at heal to regain some mana in case you're running low
- In case your shield runs and Krosh doesn't have one on him yet out you can use `Ice Barrier` (if Frost-specced), `Fire Ward` and/or Fire Protection Potion to minimize the damage. You should also let your healer know so he can pre-cast a heal/shield
- You should use any means necessary to increase your health pool (food and alcohol buffs, flasks/elixirs)
- Mages not tanking Krosh can use `Improved Counterspell` on Blindeye when he shields himself

### Paladin
- Make sure NO tank has `Blessing of Salvation` on (Mage, Balance Druid/Hunter)
- You can use `Blessing of Light` on the tank you're assigned to to help you keep him up and conserve some mana

### Priest
- Shadow Priests with `Silence` can interrupt Blindeye's `Prayer of Mending` after he shields himself
- Use `Fear Wards` to negate the impact of Maulgar's AoE fear in Phase #2 on Off-tanks
- Be careful about `Shadowfiend` positioning as it can die fairly quickly, make sure to time it in-between `Whirlwinds` so you get as much mana back as possible
- Holy Priests should keep `Power Word: Shield` on the mage tanking Krosh as much as possible to mitigate the damage, same goes for other non-standard tanks but isn't as critical

### Rogue
- `Improved Kick` can be used to interrupt Blindeye's `Prayer of Mending` after he shields himself
- When Blindeye isn't shielded you can use `Kidney Shot` to delay his shield and give extra breathing time to healers
- Make sure one of the rogues has `Mind-Numbing Poison` on his off-hand
- Additionally you can use `Wound Poison` in combination with Shiv to make sure it's stacked 5 times however `Mortal Strike`/`Aimed Shot` debuff should be applied at all times anyway

### Shaman
- Elemental Shaman should be in a group with the mage tanking Krosh as he usually has to sacrifice some spell hit for stamina, to provide him with `Totem of Wrath`
- Once Maulgar gets close to 50% drop `Tremor Totem` and make sure it stays on until he's dead
- Use `Grounding Totem` to catch Lightning Bolts from Kiggler to absorb extra damage
- Once tank establishes threat on Blindeye feel free to pop `Bloodlust` as it's essential for him to die first and not heal the rest of the bosses
- If you're in a group with Kiggler's tank(s) be sure to drop `Nature Resistance Totem` close to them

### Warlock
- Make sure all the Warlocks know who's responsible for `Enslaving` the first Fel Stalker and taunting Olm in the beginning
- Second Fel Stalker should be `Enslaved` as well and taunt Olm once the first one gets affected by `Death Coil`
- Any extra Fel Stalkers need to be `Enslaved`, `Feared` or `Banished`
- In case of 3+ Warlocks in the group the abundant Fel Stalkers can be `Enslaved` and used to taunt Krosh and killed after a few seconds which gives some breathing time to the mage tanking him and his healers
- Use `Curse of Tongues` on Blindeye to prolong his heals
- If the Mage tanking Krosh has low base HP use `Summon Imp` and have him provide `Blood Pact` buff (assuming you're in the same group). You can place him right next to the mage and let him cast `Firebolts` onto Krosh

### Warrior
- `Improved Shield Bash` can be used to interrupt Blindeye's heal when shield is active
- Warrior tanking Blindeye should consider using DPS gear due to avoidance which could potentially cause rage starvation
- Can stance-dance out of fear in Maulgar's Phase #2
- Keep `Mortal Strike` debuff on Blindeye at all times

## Positioning

![Maulgar - Positioning](maulgar.png "Maulgar - Positioning")
