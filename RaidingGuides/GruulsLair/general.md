# Gruul\'s Lair - Overview

[[_TOC_]]

## Preparation

Gruul's Lair doesn't have any attunement. Beside bringing standard consumables you can also use [Unstable Flasks](https://tbc.wowhead.com/search?q=unstable+flask) (Does it stack with regular flasks?)

**Required Raid Composition**
- 2-3 Tanks
- 6-8 Healers
- 1 Mage with high stamina gear (to tank Krosh)
- 1 Balance Druid (to tank Kiggler)
- 2 Warlocks
- 2 Hunters (in case there's no Balance Druid), ideally 5
- 1 Interrupt Class (to interrupt Blindeye's heal)

## Trash
There's very little trash mobs as Gruul's Lair only has two boss encounters however it's still important not to underestimate anything. All the trash packs before each boss are tied to defeating the boss itself and will respawn every 60 minutes after first engaged. You will meet two kinds of mobs:
- Gronn-Priest
- Lair Brute

**Gronn-Priest**
- `Heal [2 seconds]`
- `Psychic Scream [instant]`
- `Renew [instant]`

These should be your primary target in each pack, make sure shamans have Tremor Totems down and somebody has the ability to interrupt Heals. You can also purge/dispell/spellsteal the Renew.

**Lair Brutes**
- `Charge [instant]`
- `Cleave [instant, 180 degree radius]`
- `Mortal Strike [instant, applies healing reduction of 50% for 5 seconds]`

Secondary targets, melee dps should be attacking from behind to avoid Cleave damage. Lair Brutes charge into random players while dropping aggro on the tank at the same time(?). When the mob is about to die he starts calling for aid attracting other mobs in a fairly large area which means the group needs to be ready to pickup more or kill them far away from other mobs.
